#!/bin/bash
CURRENT_DIR=$CD
SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`
cd $SCRIPTPATH
venv/bin/celery -A bh beat --loglevel=debug &
venv/bin/celery -A bh worker -E --loglevel=debug &
if [ "$1x" = "servicex" ]; then
  venv/bin/celery -A bh beat --loglevel=debug --concurrency=1 --logfile=/tmp/celery.log &
  venv/bin/celery -A bh worker -E --loglevel=debug --concurrency=1 --logfile=/tmp/celery.log &
  ./managew runserver > /tmp/runserve.log &
else
  venv/bin/celery -A bh beat --loglevel=debug &
  venv/bin/celery -A bh worker -E --loglevel=debug &
  ./managew runserver
  killall -9 celery
fi
cd "${CURRENT_DIR}"
