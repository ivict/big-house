from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from bhcore.model import TestStageHistory
from bhcore.model.last_test_execution_view import LastTestExecutionView
from bhcore.model.test import Test
from bhcore.model.test_details import TestDetails
from bhcore.model.test_stage import TestStage
from bhcore.model.test_stage_status import TestStageStatus
from bhcore.model.test_statistics import TestStatistics
from bhcore.model.test_statistics_type import TestStatisticsType
from bhcore.model.test_summary import TestSummary


class TestDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDetails
        fields = '__all__'


class TestSummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSummary
        fields = '__all__'


class TestStatisticsTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestStatisticsType
        fields = '__all__'


class TestStatisticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestStatistics
        fields = '__all__'


class TestStageSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestStage
        fields = '__all__'


class TestStageStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestStageStatus
        fields = '__all__'


class TestStageHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = TestStageHistory
        fields = '__all__'


class LastTestExecutionViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = LastTestExecutionView
        fields = '__all__'


class TestSerializer(serializers.ModelSerializer):
    content = SerializerMethodField()
    summary = SerializerMethodField()

    @staticmethod
    def get_content(data):
        return data.testdetails.content if hasattr(data, 'testdetails') else None

    @staticmethod
    def get_summary(data):
        return data.testsummary.summary if hasattr(data, 'testsummary') else None

    class Meta:
        model = Test
        fields = ('mnemonic', 'name', 'content', 'summary', 'in_progress')
