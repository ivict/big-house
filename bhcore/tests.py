# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

# FOR RUN TESTS UNDER PY-CHARM-CE
# ADD DJANGO_SETTINGS_MODULE=bh.settings IN ENVIRONMENT VARIABLES FOR TESTS


class InterpreterSimpleTestCase(TestCase):
    def setUp(self):
        print("Initialized")

    def test_animals_can_speak(self):
        """Animals that can speak are correctly identified"""
        interpreter = 'I am work'
        self.assertEqual(interpreter, 'I am work')

