import os
import tempfile
import threading
from datetime import datetime
from os.path import isdir
from random import Random
from shutil import copyfile, rmtree

from bhcore.engine.docker_wrapper import DockerWrapper
from bhcore.engine.engine_error import EngineError


class TestAgentFactory:
    rnd_track = Random(datetime.now())

    def get_rnd_suffix(self):
        result = ''
        mutex = threading.Lock()
        mutex.acquire()
        for i in range(10):
            result += hex(self.next_byte(i))[2:]
        mutex.release()
        return result

    def next_byte(self, step_number):
        if (step_number // 2) % 2 != 0:
            return self.rnd_track.randint(10, 15)
        else:
            return self.rnd_track.randint(0, 9)

    def build_test_image(self, agent_image_name, docker_image_name, debug=False):
        self._validate_image_name_and_throw_if_not_valid(agent_image_name)

        # COPY DATA TEMPLATE FROM data package
        # DEFINE DATA_PATH FOLDER
        this_dir, _ = os.path.split(__file__)
        data_path = os.path.join(this_dir, "data", "agent-build", "test-11111")
        print('USE DATA PACKAGE: {data_path}'.format(data_path=data_path))

        # CREATE TEMPORARY FOLDER
        build_image_path = tempfile.mkdtemp(prefix=agent_image_name + '-', dir=os.environ['HOME'])
        print('USE TEMPORARY FOLDER FOR BUILD {build_image_path}'.format(build_image_path=build_image_path))
        try:
            # Kotlin package name
            kotlin_package_name = agent_image_name.replace('-', '_')
            replacement = dict({'test-11111': agent_image_name,
                                'test_11111': kotlin_package_name,
                                'FROM scratch': 'FROM {docker_image_name}'.format(docker_image_name=docker_image_name)})

            # COPY TEMPLATE AND REPLACE TEST NAME [test-11111] BY MNEMONIC
            # self._copy(data_path, build_image_path, ['gradle', 'wrapper', 'gradle-wrapper.jar'])
            # self._copy(data_path, build_image_path, ['gradle', 'wrapper', 'gradle-wrapper.properties'])
            self._copy_with_replacement(data_path, build_image_path,
                                        ['src', 'test_11111Main', 'kotlin', 'test_11111', 'App.kt'],
                                        replacement)
            self._copy_with_replacement(data_path, build_image_path,
                                        ['src', 'test_11111Test', 'kotlin', 'test_11111', 'AppTest.kt'],
                                        replacement)
            self._copy_with_replacement(data_path, build_image_path, ['build.gradle.kts'], replacement)
            # self._copy_with_replacement(data_path, build_image_path, ['build.sh'], replacement)
            self._copy_with_replacement(data_path, build_image_path, ['Dockerfile'], replacement)
            self._copy_with_replacement(data_path, build_image_path, ['gradle.properties'], replacement)
            # self._copy(data_path, build_image_path, ['gradlew'])
            # self._copy(data_path, build_image_path, ['gradlew.bat'])
            self._copy_with_replacement(data_path, build_image_path, ['settings.gradle.kts'], replacement)

            return DockerWrapper().build_image(build_image_path, agent_image_name)
        except IOError as e:
            raise EngineError(e)
        finally:
            if not debug:
                rmtree(build_image_path)

    @staticmethod
    def _validate_image_name_and_throw_if_not_valid(image_name):
        if ' ' in image_name:
            raise EngineError('Image name, can''t contain spaces')

    def _copy(self, from_path, to_path, relative_file_path):
        self._copy_with_replacement(from_path, to_path, relative_file_path, {})

    def _copy_with_replacement(self, from_path, to_path, relative_file_path, replacement):
        if not relative_file_path:
            raise EngineError('Invalid relative_file_path, i''s required')
        if len(relative_file_path) == 0:
            raise EngineError('Invalid relative_file_path, i''s empty')

        to_path_folder = to_path
        from_file_path = from_path
        is_file_name = len(relative_file_path)
        file_name = relative_file_path[is_file_name - 1]
        for path_element in relative_file_path:
            from_file_path = os.path.join(from_file_path, path_element)
            is_file_name -= 1
            if is_file_name > 0:
                to_path_folder = os.path.join(to_path_folder, self._replace(path_element, replacement))

        if to_path_folder != to_path and not os.path.exists(to_path_folder):
            os.makedirs(to_path_folder)
        if replacement and len(replacement) > 0:
            self._copy_file_with_replace_strings(from_file_path, os.path.join(to_path_folder, file_name), replacement)
        else:
            copyfile(from_file_path, os.path.join(to_path_folder, file_name))

    @staticmethod
    def _replace(path_element, replacement):
        if replacement and len(replacement) > 0:
            return (path_element
                    .replace('test-11111', replacement['test-11111'])
                    .replace('test_11111', replacement['test_11111']))

        return path_element

    @staticmethod
    def _copy_file_with_replace_strings(from_path, to_path, replacement):
        if isdir(from_path):
            raise EngineError('From path is directory: {from_path}'.format(from_path=from_path))

        with open(from_path, "rt") as file_in:
            with open(to_path, "wt") as file_out:
                for line in file_in:
                    file_out.write(line
                                   .replace('test-11111', replacement['test-11111'])
                                   .replace('test_11111', replacement['test_11111']))
