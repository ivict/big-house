import os
import collections
import re
import grp
import subprocess
import json

from bhcore.engine.cluster_info import ClusterInfo
from bhcore.engine.engine_error import EngineError
from bhcore.engine.node_info import NodeInfo
from bhcore.utils.parsed_table import ParsedTable

# Declaring named tuples
VersionToken = collections.namedtuple('Token', ['type', 'value', 'line', 'column'])


class DockerWrapper:
    client_api_version = ""
    client_version = ""
    client_go_version = ""
    server_api_version = ""
    server_version = ""
    server_go_version = ""

    last_command_stdout = ""
    last_command_stderr = ""

    def get_client_api_version(self):
        if self.client_api_version == "":
            self.update_version_info()
        return self.client_api_version

    def get_client_version(self):
        if self.client_version == "":
            self.update_version_info()
        return self.client_version

    def get_client_go_version(self):
        if self.client_go_version == "":
            self.update_version_info()
        return self.client_go_version

    def get_server_api_version(self):
        if self.server_api_version == "":
            self.update_version_info()
        return self.server_api_version

    def get_server_version(self):
        if self.server_version == "":
            self.update_version_info()
        return self.server_version

    def get_server_go_version(self):
        if self.server_go_version == "":
            self.update_version_info()
        return self.server_go_version

    def update_version_info(self):
        if self.client_api_version != "":
            return
        version_info = os.popen('docker version').read()
        client_server_type = 'CLIENT'
        current_type = 'CLIENT'
        version_map = collections.UserDict()
        for token in self.parse_version_info(version_info):
            if token.type in ['CLIENT', 'SERVER']:
                client_server_type = token.type
                continue
            elif token.type == 'VERSION_VALUE':
                version_map[current_type] = token.value
                continue
            else:
                current_type = client_server_type + "_" + token.type

        self.client_api_version = version_map['CLIENT_API_VERSION']
        self.client_version = version_map['CLIENT_VERSION']
        self.client_go_version = version_map['CLIENT_GO_VERSION']
        self.server_api_version = version_map['SERVER_API_VERSION']
        self.server_version = version_map['SERVER_VERSION']
        self.server_go_version = version_map['SERVER_GO_VERSION']

    # keywords = {'CLIENT', 'SERVER', 'VERSION', 'API_VERSION', 'GO_VERSION', 'VERSION_VALUE', 'NEWLINE'}
    @staticmethod
    def parse_version_info(version_info):
        token_specification = [
            ('CLIENT', r'Client\:'),  # Client token
            ('SERVER', r'Server\:'),  # Server token
            ('VERSION', r'Version\:'),  # Version token
            ('API_VERSION', r'API version\:'),  # API version token
            ('GO_VERSION', r'Go version\:'),  # Go version token
            ('VERSION_VALUE', r'\w*\d+\.\d+(\.\d+)?(\w+)?'),  # API version token
            ('NEWLINE', r'\n'),  # Next line token
        ]
        tok_regex = '|'.join('(?P<%s>%s)' % pair for pair in token_specification)
        line_num = 1
        line_start = 0
        for mo in re.finditer(tok_regex, version_info):
            kind = mo.lastgroup
            value = mo.group()
            column = mo.start() - line_start
            if kind == 'NEWLINE':
                line_start = mo.end()
                line_num += 1
                continue
            yield VersionToken(kind, value, line_num, column)

    @staticmethod
    def check_user_can_run_docker():
        return 'docker' in [grp.getgrgid(gid).gr_name for gid in os.getgroups()]

    def execute_command(self, cmd):
        process = subprocess.Popen(cmd,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   shell=True)
        (stdout, stderr) = process.communicate()
        error_status = str(stderr, "utf-8")
        norma_status = str(stdout, "utf-8")
        self.last_command_stderr = error_status
        self.last_command_stdout = norma_status
        return norma_status, error_status

    def download_docker_image(self, docker_image_name):
        (norma_status, error_status) = self.execute_command('docker image pull ' + docker_image_name)
        return 'Error' not in error_status

    def network_list(self):
        (norma_status, error_status) = self.execute_command('docker network ls')
        parsed_table = ParsedTable(norma_status, r'\s+', ['NETWORK ID', 'NAME', 'DRIVER', 'SCOPE'])
        return parsed_table.list('NAME')

    # REMOVE NETWORK BEFORE CREATE NETWORK IF NETWORK EXISTS
    def create_network(self, network_name):
        # check if network exist
        if network_name in self.network_list():
            (norma_status, error_status) = self.execute_command('docker network rm ' + network_name)
            if 'Error' in error_status:
                return False

        (norma_status, error_status) = self.execute_command('docker network create --driver bridge ' + network_name)
        return 'Error' not in error_status

    # REMOVE NODE BEFORE CREATE IF NODE EXISTS
    def create_node(self, network_name, node_name, image_name):
        (norma_status, error_status) = self.execute_command(('docker stop {node_name} || true &&'
                                                             ' docker rm {node_name} || true')
                                                            .format(node_name=node_name))
        # if 'Error' in error_status:
        #    return False

        (norma_status, error_status) = self.execute_command(('docker create --name={node_name} --network={network_name}'
                                                             ' --hostname={node_name}.{network_name} {image_name}')
                                                            .format(node_name=node_name,
                                                                    network_name=network_name,
                                                                    image_name=image_name))
        return 'Error' not in error_status

    def start_node(self, node_name):
        (norma_status, error_status) = self.execute_command('docker container start {node_name}'
                                                            .format(node_name=node_name))
        return 'Error' not in error_status

    def stop_node(self, node_name):
        (norma_status, error_status) = self.execute_command('docker container stop {node_name}'
                                                            .format(node_name=node_name))
        return 'Error' not in error_status

    def get_cluster_info(self, network_name):
        (norma_status, error_status) = self.execute_command('docker network inspect {network_name}'
                                                            .format(network_name=network_name))
        if 'Error' in error_status:
            raise EngineError('Can''t run docker network inspect command')
        return DockerWrapper.parse_network_inspect_json(norma_status)

    @staticmethod
    def parse_network_inspect_json(network_inspect_json):
        result = ClusterInfo()

        json_object = json.loads(network_inspect_json)
        info = json_object[0]
        result.id = info['Id']
        result.name = info['Name']
        result.created = info['Created']

        if info['Containers'] is not None:
            for container in info['Containers'].values():
                node = NodeInfo()
                node.name = container['Name']
                node.ip4_address = container['IPv4Address']
                result.nodes.append(node)

        return result

    def build_image(self, build_image_directory, image_name):
        (norma_status, error_status) = self.execute_command('docker build --tag {image_name} {build_image_directory}'
                                                            .format(image_name=image_name,
                                                                    build_image_directory=build_image_directory))
        success = 'failed' not in error_status
        if not success:
            raise EngineError(
                ('Failed build image {image_name} in folder {build_image_directory}\n{norma_status}\n{error_status}'
                 .format(image_name=image_name,
                         build_image_directory=build_image_directory,
                         norma_status=norma_status,
                         error_status=error_status
                         )))
        return success
