import json
import time

from celery.result import AsyncResult

from bh.celery import app as celery_app
from bhcore.engine.docker_wrapper import DockerWrapper
from bhcore.engine.engine_error import EngineError
from bhcore.engine.test_agent_factory import TestAgentFactory
from bhcore.model.compiled_scenario import CompiledScenario


def split_scenario_by_sub_tasks_and_start_them(scenario: CompiledScenario):
    # create task sequence
    request = (
            check_scenario.s(scenario) |
            check_docker_state.s() |
            download_docker_image.s() |
            setup_cluster.s() |
            create_initial_structure.s() |
            insert_initial_data.s() |
            main_scenario.s() |
            check_runtime_errors.s()
    ).apply_async()
    while not AsyncResult(request.id).status == "SUCCESS":
        time.sleep(1)
    return AsyncResult(request.id).result


def _append_scenario_errors(e, scenario):
    if not hasattr(scenario, 'runtime_errors'):
        scenario['runtime_errors'] = list()
    scenario['runtime_errors'].append(EngineError.to_json(e))


@celery_app.task
def check_scenario(scenario: CompiledScenario):
    try:
        _check_scenario(scenario)
    except EngineError as e:
        _append_scenario_errors(e, scenario)
    return scenario


def _check_scenario(scenario: CompiledScenario):
    print('######################### scenario validation')
    if scenario['docker_image'] == '_':
        raise EngineError('Invalid docker image: {docker_image}'.format(docker_image=scenario['docker_image']))
    if not scenario['mnemonic'] or len(str(scenario['mnemonic']).strip()) == 0:
        raise EngineError('Invalid mnemonic: {mnemonic}'.format(mnemonic=scenario['mnemonic']))


@celery_app.task
def check_docker_state(scenario: CompiledScenario):
    # NO SENSE TO CONTINUE
    if CompiledScenario.has_runtime_errors(scenario):
        return scenario

    try:
        _check_docker_state()
    except EngineError as e:
        _append_scenario_errors(e, scenario)
    return scenario


def _check_docker_state():
    print('######################### check docker state')
    wrapper = DockerWrapper()
    if not wrapper.check_user_can_run_docker():
        raise EngineError('Current user can''t run docker')
    client_version = wrapper.get_client_version()
    server_version = wrapper.get_server_api_version()
    if client_version < '18.00.00':
        raise EngineError('Untested version of docker client {client_version}'.format(client_version=client_version))
    if server_version < '1.00':
        raise EngineError('Invalid version of docker server {server_version}'.format(server_version=server_version))


@celery_app.task
def download_docker_image(scenario: CompiledScenario):
    if CompiledScenario.has_runtime_errors(scenario):
        return scenario

    try:
        _download_docker_images(scenario)
    except EngineError as e:
        _append_scenario_errors(e, scenario)
    return scenario


def _download_docker_images(scenario: CompiledScenario):
    print('######################### download docker image')
    wrapper = DockerWrapper()
    if not wrapper.download_docker_image(scenario['docker_image']):
        raise EngineError(
            'Errors in process of download image: {docker_image}'.format(docker_image=scenario['docker_image']))
    # BUILD AGENT LOCAL IMAGE FOR AGENT
    test_agent_factory = TestAgentFactory()
    if not test_agent_factory.build_test_image(scenario['mnemonic'],
                                               scenario['docker_image']):
        raise EngineError('Build test-agent image fail')


@celery_app.task
def setup_cluster(scenario: CompiledScenario):
    if CompiledScenario.has_runtime_errors(scenario):
        return scenario

    try:
        _setup_cluster(scenario)
    except EngineError as e:
        _append_scenario_errors(e, scenario)
    return scenario


def _setup_cluster(scenario: CompiledScenario):
    print('######################### setup cluster')
    wrapper = DockerWrapper()
    if not wrapper.create_network(scenario['mnemonic']):
        raise EngineError('Can''t create network {network_name}'.format(network_name=scenario['mnemonic']))
    print(json.dumps(wrapper.get_cluster_info(scenario['mnemonic'])))


@celery_app.task
def create_initial_structure(scenario: CompiledScenario):
    if CompiledScenario.has_runtime_errors(scenario):
        return scenario

    try:
        _create_initial_structure(scenario)
    except EngineError as e:
        _append_scenario_errors(e, scenario)
    return scenario


def _create_initial_structure(scenario: CompiledScenario):
    print('######################### create initial structure')
    wrapper = DockerWrapper()
    cluster_info = wrapper.get_cluster_info(scenario['mnemonic'])
    print(cluster_info)


@celery_app.task
def insert_initial_data(scenario: CompiledScenario):
    if CompiledScenario.has_runtime_errors(scenario):
        return scenario

    try:
        _insert_initial_data(scenario)
    except EngineError as e:
        _append_scenario_errors(e, scenario)
    return scenario


def _insert_initial_data(scenario: CompiledScenario):
    print('######################### insert initial data')
    wrapper = DockerWrapper()
    cluster_info = wrapper.get_cluster_info(scenario['mnemonic'])
    print(cluster_info)


@celery_app.task
def main_scenario(scenario: CompiledScenario):
    if CompiledScenario.has_runtime_errors(scenario):
        return scenario

    try:
        _main_scenario(scenario)
    except EngineError as e:
        _append_scenario_errors(e, scenario)
    return scenario


def _main_scenario(scenario: CompiledScenario):
    print('######################### insert initial data')
    wrapper = DockerWrapper()
    cluster_info = wrapper.get_cluster_info(scenario['mnemonic'])
    print(cluster_info)


@celery_app.task
def check_runtime_errors(scenario: CompiledScenario):
    # NO SENSE TO CONTINUE
    if CompiledScenario.has_runtime_errors(scenario):
        print('##### SCENARIO RUNTIME ERRORS:')
        print(scenario['runtime_errors'])
        return -1
    return 0
