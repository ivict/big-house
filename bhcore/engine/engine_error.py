import json


# ALL UNEXPECTED BEHAVIOR IN MODULE SCENARIO-ENGINE
class EngineError(Exception):

    @staticmethod
    def to_json(engine_error: Exception):
        return json.dumps((str(engine_error)))
