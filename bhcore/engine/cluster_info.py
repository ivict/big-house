import datetime

from bhcore.model.json_serializable import JsonSerializable


class ClusterInfo(JsonSerializable):
    id = ''
    name = ''
    created = datetime.datetime.now()
    nodes = []
