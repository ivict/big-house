package data.b.bh.agent.grpc

/**
 * Wrapper for grpc library
 */
import kotlinx.cinterop.*
import libgrpc.*

fun hello(): String = "Hello, Kotlin/Native!"


fun main() {

    println(hello())
}