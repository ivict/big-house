plugins {
    // Apply the Kotlin JVM plugin to add support for Kotlin.
    java
    id("org.jetbrains.kotlin.multiplatform")
    id("io.netifi.flatbuffers") version "1.0.6"
}

repositories {
    // Use jcenter for resolving dependencies.
    // You can declare any Maven/Ivy/file repository here.
    jcenter()
    mavenCentral()
}

kotlin {
    // Determine host preset.
    val hostOs = System.getProperty("os.name")



    // Create target for the host platform.
    val hostTarget = when {
        hostOs == "Mac OS X" -> macosX64("macos")
        hostOs == "Linux" -> linuxX64("linux") {
            val main by compilations.getting
            val libgrpc by main.cinterops.creating

            libgrpc.includeDirs.headerFilterOnly("/usr/include", "/usr/include/x86_64-linux-gnu")

            binaries {
                staticLib {
                    baseName = "grpc"
                }
                sharedLib {
                    baseName = "grpc"
                }
            }

        }
        hostOs.startsWith("Windows") -> mingwX64("mingw")
        else -> throw GradleException("Host OS '$hostOs' is not supported in Kotlin/Native $project.")
    }

    print(hostTarget)

    sourceSets {
    }

    sourceSets.all {
        languageSettings.useExperimentalAnnotation("kotlin.ExperimentalStdlibApi")
    }
}

flatbuffers {
    flatcPath = "flatc"
    language = "kotlin-native"
}

tasks.register<io.netifi.flatbuffers.plugin.tasks.FlatBuffers>("createFlatBuffers") {
    outputDir = file("src/generated/flatbuffers")
}

// Use the following Gradle tasks to run your application:
// :runReleaseExecutableLinux - without debug symbols
// :runDebugExecutableLinux - with debug symbols
