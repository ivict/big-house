package data.b

import kotlin.native.concurrent.AtomicReference
import kotlin.native.concurrent.Worker
import kotlin.system.getTimeNanos

enum class LogLevel {
    DEBUG, INFO, ERROR
}

class Logger(private val concurrency: Int = 0) {

    data class LoggerTask(val logLevel: LogLevel, val message: () -> String)

    private val fileStream = FileStream(logFile)

    /**
     * Code execution in current thread if concurrency is 0
     * Code execution in logger worker if concurrency greater then 0
     * Logger workers can be a part of other worker pools
     */
    fun debug(message: () -> String) {
        if (concurrency <= 0) {
            execute(LoggerTask(LogLevel.DEBUG, message))
            return
        }
    }

    fun info(message: () -> String) {
        if (concurrency <= 0) {
            execute(LoggerTask(LogLevel.INFO, message))
            return
        }
    }

    fun error(message: () -> String) {
        if (concurrency <= 0) {
            execute(LoggerTask(LogLevel.ERROR, message))
            return
        }
    }

    @UseExperimental(ExperimentalUnsignedTypes::class)
    private fun execute(loggerTask: LoggerTask) {
        val message = "[${loggerTask.logLevel}] ${loggerTask.message()}"
        println(message)
        fileStream.appendText(message)
    }

    companion object {
        private const val logFile = "./application.log"
        private val systemLogger = Logger()

        fun debug(message: String) {
            systemLogger.debug { message }
        }
        fun info(message: String) {
            systemLogger.info { message }
        }
        fun error(message: String) {
            systemLogger.error { message }
        }
    }
}