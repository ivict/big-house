package data.b

import platform.posix.*
import kotlin.native.concurrent.AtomicLong

class FileStream(private val fileName: String) {

    var pos: AtomicLong = AtomicLong(fileStreamPosition());

    private fun fileStreamPosition(): Long {
        val fd = open(fileName, O_RDONLY)
        try {
            return lseek(fd, 0L, SEEK_END)
        } finally {
            close(fd)
        }
    }

    @ExperimentalUnsignedTypes
    fun appendText(text: String) {
        val fileStream = fopen(fileName, "ab")
        try {
            val line = text + "\n"
            val textAsByteArray = line.encodeToByteArray()
            val lastPosition = pos.addAndGet(textAsByteArray.size)
            println("Set position: ${lastPosition - textAsByteArray.size}")
            fseek(fileStream, lastPosition - textAsByteArray.size, SEEK_CUR)
            fputs(line, fileStream)
            fflush(fileStream)
        } finally {
            fclose(fileStream)
        }
    }
}