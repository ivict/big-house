# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# -*- initialization test env -*-
from bh.tests import TestsBasic
from bhcore.engine.docker_wrapper import DockerWrapper


class TestsDockerWrapper(TestsBasic):
    def setUp(self):
        print("Initialized")

    def test_docker_version(self):
        wrapper = DockerWrapper()
        wrapper.update_version_info()
        print(wrapper.client_api_version)
        print(wrapper.client_version)
        print(wrapper.server_api_version)
        print(wrapper.server_version)
        self.assertTrue(wrapper.client_api_version)

    def test_parse_version_info(self):
        for token in DockerWrapper.parse_version_info("""
Client:
 Version:           18.09.7
 API version:       1.38 (downgraded from 1.39)
 Go version:        go1.10.1
 Git commit:        2d0083d
 Built:             Fri Aug 16 14:20:06 2019
 OS/Arch:           linux/amd64
 Experimental:      false

Server:
 Engine:
  Version:          18.06.1-ce
  API version:      1.38 (minimum version 1.12)
  Go version:       go1.10.4
  Git commit:       e68fc7a
  Built:            Tue May  7 17:57:34 2019
  OS/Arch:          linux/amd64
  Experimental:     false        
        """):
            print(token)
            self.assertTrue(token)

    def test_get_client_version(self):
        wrapper = DockerWrapper()
        version = wrapper.get_client_version()
        print('#############################################')
        print(version)
        print('---------------------------------------------')
        self.assertTrue(version)

    def test_check_user_can_run_docker(self):
        value = DockerWrapper.check_user_can_run_docker()
        self.assertTrue(value is not None)

    def test_success_download_docker_image(self):
        wrapper = DockerWrapper()
        result = wrapper.download_docker_image('hello-world')
        print('OUTPUT: ' + wrapper.last_command_stdout)
        print('ERRORS: ' + wrapper.last_command_stderr)
        self.assertTrue(result)

    def test_fail_download_docker_image(self):
        wrapper = DockerWrapper()
        result = wrapper.download_docker_image('unknown265370668796565410079577231595685990246252183923'
                                               '4713497644848683294080338974672799215511652271165188178'
                                               '6509842972779110178575286460734332343694412049811214638'
                                               '4872530459282383531546581969613793')
        print('OUTPUT: ' + wrapper.last_command_stdout)
        print('ERRORS: ' + wrapper.last_command_stderr)
        self.assertFalse(result)

    def test_create_network(self):
        wrapper = DockerWrapper()
        result = wrapper.create_network("test-11111")
        print('OUTPUT: ' + wrapper.last_command_stdout)
        print('ERRORS: ' + wrapper.last_command_stderr)
        self.assertTrue(result)

    def test_create_node(self):
        wrapper = DockerWrapper()
        result = wrapper.create_node("test-11111", "clh1", "hello-world")
        print('OUTPUT: ' + wrapper.last_command_stdout)
        print('ERRORS: ' + wrapper.last_command_stderr)
        self.assertTrue(result)

    # # NOT UNIT TEST
    # def test_run_node(self):
    #     wrapper = DockerWrapper()
    #     create_node_result = wrapper.create_node("test-11111", "clh2", "oracle/nosql")
    #     print('OUTPUT: ' + wrapper.last_command_stdout)
    #     print('ERRORS: ' + wrapper.last_command_stderr)
    #     self.assertTrue(create_node_result)
    #     result = wrapper.run_node("clh2")
    #     print('OUTPUT: ' + wrapper.last_command_stdout)
    #     print('ERRORS: ' + wrapper.last_command_stderr)
    #     self.assertTrue(result)

    # # NOT UNIT TEST
    # def test_stop_node(self):
    #     wrapper = DockerWrapper()
    #     create_node_result = wrapper.create_node("test-11111", "clh2", "oracle/nosql")
    #     print('OUTPUT: ' + wrapper.last_command_stdout)
    #     print('ERRORS: ' + wrapper.last_command_stderr)
    #     self.assertTrue(create_node_result)
    #     result = wrapper.start_node("clh2")
    #     print('OUTPUT: ' + wrapper.last_command_stdout)
    #     print('ERRORS: ' + wrapper.last_command_stderr)
    #     self.assertTrue(result)
    #     result = wrapper.stop_node("clh2")
    #     print('OUTPUT: ' + wrapper.last_command_stdout)
    #     print('ERRORS: ' + wrapper.last_command_stderr)
    #     self.assertTrue(result)

    def test_parse_network_inspect_json(self):
        network_inspect_json = """
[
    {
        "Name": "test-11111",
        "Id": "4abd4ed9003119d4917825a30b45bafe5fe8d74e897af4de3007c24f70610ecd",
        "Created": "2019-10-28T21:33:53.568538436+03:00",
        "Scope": "local",
        "Driver": "bridge",
        "EnableIPv6": false,
        "IPAM": {
            "Driver": "default",
            "Options": {},
            "Config": [
                {
                    "Subnet": "172.21.0.0/16",
                    "Gateway": "172.21.0.1"
                }
            ]
        },
        "Internal": false,
        "Attachable": false,
        "Ingress": false,
        "ConfigFrom": {
            "Network": ""
        },
        "ConfigOnly": false,
        "Containers": {
            "f344988ef8c27cadc48ed803cbdc43e58c657d237be9f03998c0dd7b27e93a00": {
                "Name": "clh2",
                "EndpointID": "b8ad62f1fa112c197bb7e8a4d253896afa984d40daf6a0954b24da3c28d20eba",
                "MacAddress": "02:42:ac:15:00:02",
                "IPv4Address": "172.21.0.2/16",
                "IPv6Address": ""
            }
        },
        "Options": {},
        "Labels": {}
    }
]
        """
        result = DockerWrapper.parse_network_inspect_json(network_inspect_json)
        self.assertTrue(result is not None)
        self.assertEquals(result.name, 'test-11111')
        self.assertTrue(result.nodes is not None)
        self.assertEquals(len(result.nodes), 1)
