# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# -*- initialization test env -*-
from bh.tests import TestsBasic
from bhcore.engine.test_agent_factory import TestAgentFactory


class TestsTestAgentFactory(TestsBasic):
    def setUp(self):
        print("Initialized")

    def test_get_rnd_suffix(self):
        factory = TestAgentFactory()
        suffix = factory.get_rnd_suffix()
        print('RANDOM SUFFIX: ' + suffix)
        self.assertTrue(suffix is not None)

    def test_build_test_image(self):
        factory = TestAgentFactory()
        suffix = factory.get_rnd_suffix()
        image_name = 'bh-test-' + suffix

        # FOR ENABLING DEBUG set debug=True
        # That action keep temporary directory
        self.assertTrue(factory.build_test_image(image_name, 'scratch', debug=True))
