# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# -*- initialization test env -*-
import time
from time import sleep

from bh.tests import TestsBasic
from bhcore.engine.scenario_executor import split_scenario_by_sub_tasks_and_start_them
from bhcore.model.compiled_scenario import CompiledScenario


class TestsExecutor(TestsBasic):

    def test_split_scenario_by_sub_tasks_and_start_them(self):
        scenario = CompiledScenario()
        scenario.mnemonic = 'test-11111'
        scenario.docker_image = 'oracle/nosql'
        scenario.cluster_nodes = 2
        scenario.replication_factor = 2
        scenario.data_model = 'create table1 (id int, name varchar(255));'
        scenario.initial_data = 'insert into table1 values (random-data-10000);'
        scenario.scenario = 'select * from table1 by row basis;'

        self.assertEqual(0, split_scenario_by_sub_tasks_and_start_them(scenario))  # .s(scenario).apply())
        print('Positive test [PASSED]')

    def test_split_scenario_by_sub_tasks_and_start_them_negative_case(self):
        scenario = CompiledScenario()
        scenario.docker_image = '_'
        scenario.cluster_nodes = 2
        scenario.replication_factor = 2

        self.assertEqual(-1, split_scenario_by_sub_tasks_and_start_them(scenario))  # .s(scenario).apply())
        print('Negative test [PASSED]')
