# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from rest_framework import filters, viewsets

from bhcore.model.test_statistics import TestStatistics
from bhcore.serializer import TestStatisticsSerializer


class TestStatisticsAPIViewSet(viewsets.ModelViewSet):
    filter_backends = (filters.SearchFilter,)
    queryset = TestStatistics.objects.all()
    serializer_class = TestStatisticsSerializer
