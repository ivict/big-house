# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from rest_framework import filters, viewsets

from bhcore.model.test_details import TestDetails
from bhcore.serializer import TestDetailsSerializer


class TestDetailsAPIViewSet(viewsets.ModelViewSet):
    search_fields = ['content']
    filter_backends = (filters.SearchFilter,)
    queryset = TestDetails.objects.all()
    serializer_class = TestDetailsSerializer
