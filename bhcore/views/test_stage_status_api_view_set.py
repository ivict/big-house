# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from rest_framework import filters, viewsets

from bhcore.model.test_stage_status import TestStageStatus
from bhcore.serializer import TestStageStatusSerializer


class TestStageStatusAPIViewSet(viewsets.ModelViewSet):
    search_fields = ['name']
    filter_backends = (filters.SearchFilter,)
    queryset = TestStageStatus.objects.all()
    serializer_class = TestStageStatusSerializer
