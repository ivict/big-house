# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from rest_framework import filters, viewsets

from bhcore.model.test_statistics_type import TestStatisticsType
from bhcore.serializer import TestStatisticsTypeSerializer


class TestStatisticsTypeAPIViewSet(viewsets.ModelViewSet):
    search_fields = ['name']
    filter_backends = (filters.SearchFilter,)
    queryset = TestStatisticsType.objects.all()
    serializer_class = TestStatisticsTypeSerializer
