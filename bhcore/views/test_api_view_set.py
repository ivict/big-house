# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from rest_framework import filters, viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from bhcore.model import TestSummary
from bhcore.model.test import Test
from bhcore.model.test_details import TestDetails
from bhcore.serializer import TestSerializer
from bhcore.services.compile_summary import CompileSummary
from bhcore.services.run_test import RunTest


class TestAPIViewSet(viewsets.ModelViewSet):
    search_fields = ['name', 'mnemonic']
    filter_backends = (filters.SearchFilter,)
    queryset = Test.objects.all().prefetch_related('testdetails', 'testsummary')
    serializer_class = TestSerializer

    def get_object(self):
        return super().get_object()

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        validated_data = request.data
        initial_mnemonic = validated_data.get('initial_mnemonic', None)
        if initial_mnemonic is not None and initial_mnemonic != validated_data.get('mnemonic', None):
            raise Exception('Now mnemonic would not be change [you could to acquisition additional functional]\n' +
                            initial_mnemonic + ' => ' + validated_data.get('mnemonic', None))
        test = Test.objects.create_or_update(mnemonic=validated_data.get('mnemonic', None),
                                             name=validated_data.get('name', None))
        if validated_data.get('content', None):
            TestDetails.objects.create_or_update_details(test_id=test.id,
                                                         content=validated_data.get('content', None))
            TestSummary.objects.create_or_update_summary(test_id=test.id,
                                                         summary=CompileSummary.execute(validated_data))
        serializer = self.get_serializer(data=validated_data)
        serializer.is_valid()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @action(detail=False, methods=['post'])
    def run(self, request, *args, **kwargs):
        validated_data = request.data
        validated_data['test_started'] = RunTest.execute(validated_data)
        serializer = self.get_serializer(data=validated_data)
        serializer.is_valid()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_202_ACCEPTED, headers=headers)
