# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from rest_framework import filters, viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from bhcore.model import Test, TestStageHistory
from bhcore.model.last_test_execution_view import LastTestExecutionView
from bhcore.serializer import TestStageHistorySerializer, LastTestExecutionViewSerializer


class TestStageHistoryAPIViewSet(viewsets.ModelViewSet):
    search_fields = ['test_mnemonic']
    filter_backends = (filters.SearchFilter,)
    queryset = TestStageHistory.objects.all()
    serializer_class = TestStageHistorySerializer

    @action(detail=False, url_path='last/(?P<mnemonic>[^/.]+)')
    def last(self, request, mnemonic):
        test = Test.objects.filter(mnemonic=mnemonic).first()
        data = LastTestExecutionViewSerializer(LastTestExecutionView.objects.filter(test_id=test.id), many=True).data
        headers = self.get_success_headers(data)
        return Response(data, status=status.HTTP_202_ACCEPTED, headers=headers)
