# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from rest_framework import filters, viewsets

from bhcore.model.test_stage import TestStage
from bhcore.serializer import TestStageSerializer


class TestStageAPIViewSet(viewsets.ModelViewSet):
    search_fields = ['name']
    filter_backends = (filters.SearchFilter,)
    queryset = TestStage.objects.all()
    serializer_class = TestStageSerializer
