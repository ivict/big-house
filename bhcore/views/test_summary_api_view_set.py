# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from rest_framework import filters, viewsets

from bhcore.model.test_summary import TestSummary
from bhcore.serializer import TestSummarySerializer


class TestSummaryAPIViewSet(viewsets.ModelViewSet):
    search_fields = ['summary']
    filter_backends = (filters.SearchFilter,)
    queryset = TestSummary.objects.all()
    serializer_class = TestSummarySerializer
