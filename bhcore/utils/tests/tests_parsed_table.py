# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

from bhcore.utils.parsed_table import ParsedTable


class TestsParsedTable(TestCase):
    def setUp(self):
        print("Initialized")

    def test_simple_parsed_table(self):
        parsed_table = ParsedTable("""
FILE1\tSIZE
test.xml\t12M
test.txt\t14M
test.bin\t1M
test\t1M
.test\tDIR
""", r'\t', ['FILE1'])
        print(parsed_table)
        self.assertEquals(len(parsed_table.rows), 5)

    def test_list_in_parsed_table(self):
        parsed_table = ParsedTable("""
NETWORK ID          NAME                DRIVER              SCOPE
35ebc1c4d55e        bridge              bridge              local
28d513aabd9a        host                host                local
a0e84fefa542        none                null                local
33d93a5192a3        test-11111           bridge              local
""", r'\s+', ['NETWORK ID', 'NAME', 'DRIVER', 'SCOPE'])
        print(parsed_table)
        names = parsed_table.list('NAME')
        self.assertEquals(len(names), 4)
        self.assertTrue('test-11111' in names)
        self.assertTrue('bridge' in names)
        self.assertTrue('host' in names)
        self.assertTrue('none' in names)

