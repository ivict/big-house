import re


class ParsedTable:
    headers = None
    rows = None

    def __init__(self, string, tab_regex, headers_array):
        self.headers = dict()
        self.rows = list()
        first_line = True
        for next_line in re.split(r'\r\n|\n\r|\n|\r', string):
            if next_line == '':                     # OMIT EMPTY LINES
                continue
            if first_line:                          # PARSE HEADERS
                header_set = set(headers_array)
                index = 0
                for match in re.finditer(re.compile('|'.join(headers_array) + '|\\w+'), next_line):
                    index += 1
                    if match.group() in header_set:
                        self.headers[match.group()] = index
                first_line = False
            else:
                self.rows.append(re.split(tab_regex, next_line))

    def list(self, header):
        index = self.headers[header]
        result = list()
        for row in self.rows:
            result.append(row[index - 1])
        return result
