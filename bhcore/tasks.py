from datetime import datetime, timedelta

from bh.celery import app
from bhcore.model import Test

from bh.celery import app as celery_app

# import pytz
# from django_celery_beat.models import PeriodicTask, IntervalSchedule, CrontabSchedule
#
# schedule1, _ = IntervalSchedule.objects.get_or_create(
#     every=10,
#     period=IntervalSchedule.SECONDS,
# )
#
# schedule2, _ = CrontabSchedule.objects.get_or_create(
#     minute='1',
#     hour='*',
#     day_of_week='*',
#     day_of_month='*',
#     month_of_year='*',
#     timezone=pytz.timezone('UTC')
# )
#
# PeriodicTask.objects.update(
#     crontab=schedule2,
#     name='Importing contacts',
#     task='bhcore.tasks.test',
# )
#
from bhcore.model.last_test_execution_statistics_view import LastTestExecutionStatisticsView


@celery_app.on_after_finalize.connect
def setup_periodic_tasks(**kwargs):
    # Calls check test states every 30 seconds
    app.add_periodic_task(30.0, check_living_tests.s('OK'), expires=10)

    # Calls check test states every 5 seconds
    # app.add_periodic_task(5.0, test.s(datetime.now() - timedelta(seconds=30)), expires=10)


@celery_app.task
def check_living_tests(arg, **kwargs):
    for history in LastTestExecutionStatisticsView.objects.filter(test__in_progress=True,
                                                                  start_execution_stamp__isnull=False).distinct():
        if history.start_execution_stamp.replace(tzinfo=None) > datetime.now() - timedelta(seconds=30):
            continue
        print('Outdated test {test}, reset in_progress flag'.format(test=history.test))
        in_progress_test = history.test
        in_progress_test.in_progress = False
        in_progress_test.save(update_fields=['in_progress'])


@celery_app.task
def test(arg):
    print(arg)
