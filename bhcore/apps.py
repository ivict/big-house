# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class BhCoreConfig(AppConfig):
    name = 'bhcore'
    verbose_name = "Big House Core Application"

    def ready(self):
        print("BhCoreConfig.ready invoked.")
