# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class BaseModel(models.Model):
    class Meta:
        abstract = True
        verbose_name = 'BaseModel'
        permissions = [("can_view", "Can view {something}".format(something=verbose_name)),
                       ("can_add", "Can add {something}".format(something=verbose_name)),
                       ("can_delete", "Can delete {something}".format(something=verbose_name)),
                       ("can_set", "Can set {something}".format(something=verbose_name)),
                       ]
