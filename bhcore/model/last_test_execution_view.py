# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from bhcore.model.test import Test


class LastTestExecutionViewManager(models.Manager):
    pass


# LAST EXECUTION VIEW
class LastTestExecutionView(models.Model):
    test = models.OneToOneField(
        Test,
        primary_key=True,
        db_column='test_id',
        on_delete=models.DO_NOTHING,
    )
    mnemonic = models.CharField(max_length=32)
    stage_mnemonic = models.CharField(max_length=32)
    stage_name = models.CharField(max_length=255)
    stage_status_mnemonic = models.CharField(max_length=32)
    stage_status_name = models.CharField(max_length=255)

    objects = LastTestExecutionViewManager()

    class Meta:
        managed = False
        db_table = u'LAST_TEST_EXECUTION_VIEW'
