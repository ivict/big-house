# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from bhcore.model.base_model import BaseModel


class TestStageManager(models.Manager):
    pass


# BASIC CONCEPT
class TestStage(BaseModel):
    id = models.SmallIntegerField(primary_key=True)
    mnemonic = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=255)

    objects = TestStageManager()

    class Meta(BaseModel.Meta):
        db_table = u'TEST_STAGE'
