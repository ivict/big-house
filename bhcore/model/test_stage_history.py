# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.db import models

from bhcore.model.base_model import BaseModel
from bhcore.model.test_stage_status import TestStageStatus
from bhcore.model.test import Test
from bhcore.model.test_stage import TestStage


class TestStageHistoryManager(models.Manager):
    pass


# BASIC CONCEPT
class TestStageHistory(BaseModel):
    id = models.BigAutoField(primary_key=True)
    test = models.ForeignKey(
        Test,
        db_column='test_id',
        on_delete=models.CASCADE,
    )
    test_stage = models.ForeignKey(
        TestStage,
        db_column='test_stage_id',
        on_delete=models.DO_NOTHING,
    )
    stamp = models.DateTimeField(default=datetime.now)
    test_stage_status = models.ForeignKey(
        TestStageStatus,
        db_column='test_stage_status_id',
        on_delete=models.DO_NOTHING,
        null=True
    )

    objects = TestStageHistoryManager()

    class Meta(BaseModel.Meta):
        db_table = u'TEST_STAGE_HISTORY'
