# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from bhcore.model.base_model import BaseModel


class TestManager(models.Manager):
    def check_exists(self, mnemonic):
        test = self.filter(mnemonic=mnemonic).count()
        return test > 0

    def create_or_update(self, mnemonic, name):
        test = self.filter(mnemonic=mnemonic).first()
        if test is not None:
            self.filter(pk=test.id).update(name=name)
            return test
        else:
            return self.create(mnemonic=mnemonic,
                               name=name)


class Test(BaseModel):
    id = models.BigAutoField(primary_key=True)
    mnemonic = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=255)
    in_progress = models.BooleanField(default=False)

    objects = TestManager()

    class Meta(BaseModel.Meta):
        db_table = u'TEST'
