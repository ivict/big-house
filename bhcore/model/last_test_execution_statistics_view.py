# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from bhcore.model.test import Test


class LastTestExecutionStatisticsViewManager(models.Manager):
    pass


# LAST EXECUTION STATISTICS VIEW
class LastTestExecutionStatisticsView(models.Model):
    test = models.OneToOneField(
        Test,
        primary_key=True,
        db_column='test_id',
        on_delete=models.DO_NOTHING,
    )
    mnemonic = models.CharField(max_length=32)
    start_execution_stamp = models.DateTimeField()
    statistics_type_mnemonic = models.CharField(max_length=32)
    statistics_type_name = models.CharField(max_length=255)
    value = models.FloatField()

    objects = LastTestExecutionStatisticsViewManager()

    class Meta:
        managed = False
        db_table = u'LAST_TEST_EXECUTION_STAT_VIEW'
