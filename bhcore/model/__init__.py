# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from bhcore.model.test import Test
from bhcore.model.test_details import TestDetails
from bhcore.model.test_stage import TestStage
from bhcore.model.test_stage_history import TestStageHistory
from bhcore.model.test_stage_status import TestStageStatus
from bhcore.model.test_statistics import TestStatistics
from bhcore.model.test_statistics_type import TestStatisticsType
from bhcore.model.test_summary import TestSummary

# REGISTERING FOR EASY TESTING
admin.site.register(Test)
admin.site.register(TestDetails)
admin.site.register(TestSummary)
admin.site.register(TestStatisticsType)
admin.site.register(TestStatistics)
admin.site.register(TestStage)
admin.site.register(TestStageStatus)
admin.site.register(TestStageHistory)
