# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from bhcore.model import TestStageHistory
from bhcore.model.base_model import BaseModel
from bhcore.model.test_statistics_type import TestStatisticsType


class TestStatisticsManager(models.Manager):
    pass


# BASIC CONCEPT
class TestStatistics(BaseModel):
    test_stage_history = models.OneToOneField(
        TestStageHistory,
        primary_key=True,
        db_column='test_stage_history_id',
        on_delete=models.CASCADE,
    )
    test_statistics_type = models.ForeignKey(
        TestStatisticsType,
        db_column='test_statistics_type_id',
        on_delete=models.CASCADE,
    )
    value = models.FloatField()

    objects = TestStatisticsManager()

    class Meta(BaseModel.Meta):
        db_table = u'TEST_STATISTICS'
