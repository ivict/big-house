# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from bhcore.model.base_model import BaseModel
from bhcore.model.test import Test


class TestDetailsManager(models.Manager):

    def create_or_update_details(self, test_id, content):
        test_details = self.filter(test_id=test_id)
        if test_details:
            self.filter(pk=test_id).update(content=content)
        else:
            self.create(pk=test_id,
                        test_id=test_id,
                        content=content)


# VERTICAL SCALE FOR BASIC CONCEPT [TEST]
class TestDetails(BaseModel):
    test = models.OneToOneField(
        Test,
        db_column='test_id',
        on_delete=models.CASCADE,
        primary_key=True
    )
    content = models.TextField()

    objects = TestDetailsManager()

    class Meta(BaseModel.Meta):
        db_table = u'TEST_DETAILS'
