# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from bhcore.model.base_model import BaseModel
from bhcore.model.test import Test


class TestSummaryManager(models.Manager):

    def create_or_update_summary(self, test_id, summary):
        test_summary = self.filter(test_id=test_id)
        if test_summary:
            self.filter(pk=test_id).update(summary=summary)
        else:
            self.create(pk=test_id,
                        test_id=test_id,
                        summary=summary)


# VERTICAL SCALE FOR BASIC CONCEPT [TEST]
class TestSummary(BaseModel):
    test = models.OneToOneField(
        Test,
        db_column='test_id',
        on_delete=models.CASCADE,
        primary_key=True
    )
    summary = models.CharField(max_length=4000)

    objects = TestSummaryManager()

    class Meta(BaseModel.Meta):
        db_table = u'TEST_SUMMARY'
