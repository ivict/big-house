from bhcore.model.json_serializable import JsonSerializable


class CompiledScenario(JsonSerializable):
    mnemonic = ''
    docker_image = ''
    cluster_nodes = 0
    replication_factor = 0
    data_model = ''
    initial_data = ''
    scenario = ''
    runtime_errors = []

    @staticmethod
    def has_runtime_errors(scenario):
        return scenario['runtime_errors'] and len(scenario['runtime_errors']) > 0
