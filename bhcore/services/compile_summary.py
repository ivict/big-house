import re

from django import forms
from service_objects.services import Service

from bhcore.services.compile_scenario import CompileScenario


class CompileSummary(CompileScenario):
    content = forms.CharField()

    def process(self):
        scenario = self.process()

        summary = 'input: docker image ' + scenario.docker_image
        summary += '\ncluster nodes: ' + scenario.cluster_nodes
        summary += '\nreplication factor: ' + scenario.replication_factor
        return summary
