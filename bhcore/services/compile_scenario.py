import re

from django import forms
from service_objects.services import Service

from bhcore.model.compiled_scenario import CompiledScenario


class CompileScenario(Service):
    content = forms.CharField()
    not_present = (-1, -1)

    def process(self):
        content = self.cleaned_data['content']
        sections = dict()
        docker_image = re.search('(docker(\\s+|-)image(\\s*):)', content, re.MULTILINE)
        sections[docker_image] = self._interval_or_not_present(docker_image)
        cluster_nodes = re.search('(cluster(\\s+|-)nodes(\\s*):)', content, re.MULTILINE)
        sections[cluster_nodes] = self._interval_or_not_present(cluster_nodes)
        replication_factor = re.search('(replication(\\s*):)', content, re.MULTILINE)
        sections[replication_factor] = self._interval_or_not_present(replication_factor)
        data_model = re.search('(data(\\s+|-)model(\\s*):)', content, re.MULTILINE)
        sections[data_model] = self._interval_or_not_present(data_model)
        initial_data = re.search('(initial(\\s+|-)data(\\s*):)', content, re.MULTILINE)
        sections[initial_data] = self._interval_or_not_present(initial_data)
        scenario = re.search('(scenario(\\s*):)', content, re.MULTILINE)
        sections[scenario] = self._interval_or_not_present(scenario)

        reverse_start_index = {v[0]: k for k, v in sections.items()}
        sorted_positions = list(sections.values())
        sorted_positions.sort()
        prev_pos = 0
        for pos_end_pos in sorted_positions:
            if pos_end_pos[0] <= 0:
                continue
            # UPGRADE SECTION POSITIONS
            section = reverse_start_index[prev_pos]
            interval = sections[section]
            sections[section] = (interval[1], pos_end_pos[0])

            prev_pos = pos_end_pos[0]

        # UPGRADE LAST POSITION
        if prev_pos > 0:
            section = reverse_start_index[prev_pos]
            interval = sections[section]
            sections[section] = (interval[1], len(content))

        compiled_scenario = CompiledScenario()

        compiled_scenario.docker_image = (self._value_or_none(sections, docker_image, content) or '_').strip()
        compiled_scenario.cluster_nodes = int(self._value_or_none(sections, cluster_nodes, content) or '0')
        compiled_scenario.replication_factor = int(self._value_or_none(sections, replication_factor, content) or '0')
        compiled_scenario.data_model = (self._value_or_none(sections, data_model, content) or '').strip()
        compiled_scenario.initial_data = (self._value_or_none(sections, initial_data, content) or '').strip()
        compiled_scenario.scenario = (self._value_or_none(sections, scenario, content) or '').strip()
        return compiled_scenario

    @staticmethod
    def _interval_or_not_present(matcher):
        return (matcher.start(1), matcher.end(1)) if matcher else CompileScenario.not_present

    @staticmethod
    def _value_or_none(sections, section, content):
        interval = sections[section]
        if interval[0] == interval[1]:
            return None
        return content[interval[0]:interval[1]]
