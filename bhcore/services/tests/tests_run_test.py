import unittest

from bh.tests import TestsBasic
from bhcore.model import Test, TestDetails
from bhcore.services.run_test import RunTest


# -*- initialization test env -*-


class TestsRunTest(TestsBasic):

    def test_process(self):
        # Prepare data
        test = Test.objects.create_or_update('test-11111', 'Oracle nosql testing')
        TestDetails.objects.create_or_update_details(test.id, '''
            docker image: oracle/nosql
            cluster nodes: 2
            replication: 2

            data-model:
                create table1 (id int, name varchar(255));
 
            initial-data:
                insert into table1 values (random-data-10000);

            scenario:
                select * from table1 by row basis;

            ''')

        response = RunTest.execute({
            'mnemonic': 'test-11111'
        })
        self.assertEqual('{"test_started": true}', response)


if __name__ == '__main__':
    unittest.main()
