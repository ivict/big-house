import unittest

# -*- initialization test env -*-
from bh.tests import TestsBasic
from bhcore.services.compile_scenario import CompileScenario


class TestsCompileScenario(TestsBasic):

    def test_process(self):
        scenario = CompileScenario.execute({
            'content': '''
            docker image: oracle/nosql
            cluster nodes: 2
            replication: 2

            data-model:
                create table1 (id int, name varchar(255));
 
            initial-data:
                insert into table1 values (random-data-10000);

            scenario:
                select * from table1 by row basis;

            '''
        })

        # Use check
        # scenario = CompiledScenario()

        self.assertEqual(2, scenario.cluster_nodes)
        self.assertEqual(2, scenario.replication_factor)
        self.assertEqual('oracle/nosql', scenario.docker_image)
        self.assertEqual('create table1 (id int, name varchar(255));', scenario.data_model)
        self.assertEqual('insert into table1 values (random-data-10000);', scenario.initial_data)
        self.assertEqual('select * from table1 by row basis;', scenario.scenario)


if __name__ == '__main__':
    unittest.main()
