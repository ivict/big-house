from django import forms
from service_objects.services import Service

from bhcore.engine.scenario_executor import split_scenario_by_sub_tasks_and_start_them
from bhcore.model import Test, TestDetails, TestStageHistory, TestStage, TestStatistics, TestStatisticsType
from bhcore.services.compile_scenario import CompileScenario


class RunTest(Service):
    mnemonic = forms.CharField()

    @staticmethod
    def run_test(test, scenario):
        # Mark test in progress for front-end
        test.in_progress = True
        test.save(update_fields=['in_progress'])
        # Check in execution of test
        test_stage = TestStage.objects.filter(mnemonic='statistics-gathering').first()
        test_stage_history = TestStageHistory.objects.create(test=test,
                                                             test_stage=test_stage)
        test_statistics_type = TestStatisticsType.objects.filter(mnemonic='run-time').first()
        TestStatistics.objects.create(test_stage_history=test_stage_history,
                                      test_statistics_type=test_statistics_type,
                                      value=0.0)
        split_scenario_by_sub_tasks_and_start_them(scenario)
        return 'Scenario [' + test.mnemonic + '] started'

    def process(self):
        mnemonic = self.cleaned_data['mnemonic']
        test = Test.objects.filter(mnemonic=mnemonic).first()
        if not test:
            raise Exception('Test not found by mnemonic: ' + mnemonic)
        test_details = TestDetails.objects.filter(pk=test.id).first()
        if not test_details:
            raise Exception('TestDetails not found by id: ' + test.id)
        print('Compile and run test: ' + test_details.content)
        scenario = CompileScenario.execute({'content': test_details.content})
        r = self.run_test(test, scenario)
        print(r)
        return '{"test_started": true}'
