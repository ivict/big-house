# Generated by Django 2.2.6 on 2019-11-18 09:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bhcore', '0002_insert_data_25Oct2015'),
    ]

    operations = [
        migrations.AlterField(
            model_name='test',
            name='id',
            field=models.BigAutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='testdetails',
            name='test',
            field=models.OneToOneField(db_column='test_id', on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='bhcore.Test'),
        ),
        migrations.AlterField(
            model_name='teststagehistory',
            name='id',
            field=models.BigAutoField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='testsummary',
            name='test',
            field=models.OneToOneField(db_column='test_id', on_delete=django.db.models.deletion.CASCADE, primary_key=True, serialize=False, to='bhcore.Test'),
        ),
    ]
