import 'dart:async';
import 'dart:convert';

import 'package:bhfront/events/buses/console_bus.dart';
import 'package:bhfront/utils/constants.dart';
import 'package:bhfront/utils/profile.dart';
import 'package:universal_io/prefer_universal/io.dart';

class TestService {
  static final ConsoleBus consoleBus = ConsoleBus();

  static Future<String> searchTests(String query) async {
    final String hostUrl = getStringSetting(URL_KEY, URL_KEY_DEFAULT_VALUE);
    String url = '$hostUrl/api/test/?format=json&search=$query';

    final httpClient = HttpClient();
    final request = await httpClient.openUrl('GET', Uri.parse(url));
    request.headers.add('Content-type', 'application/json');
    request.headers.add('Accept', 'application/json');
    request.headers.add('Authorization', 'Basic YWRtaW46cG9zdGdyZXM=');
    final response = request.close();
    return response.then((value) {
      var completer = Completer<String>();
      value.transform(utf8.decoder).listen((contents) {
        completer.complete(contents);
      });
      return completer.future;
    });
  }

  static Future<String> saveTest(dynamic data) async {
    final String hostUrl = getStringSetting(URL_KEY, URL_KEY_DEFAULT_VALUE);
    String url = '$hostUrl/api/test/';

    final httpClient = HttpClient();
    final request = await httpClient.openUrl('POST', Uri.parse(url));
    request.headers.add('Content-type', 'application/json');
    request.headers.add('Accept', 'application/json');
    request.headers.add('Authorization', 'Basic YWRtaW46cG9zdGdyZXM=');
    var result = jsonEncode(data);
    request.write(result);
    final response = request.close();
    return response.then((value) {
      var completer = Completer<String>();
      value.transform(utf8.decoder).listen((contents) {
        if (value.statusCode < 200 || (value.statusCode >= 300 && value.statusCode < 500)) {
          completer.complete('{"statusCode": ${value.statusCode}, "error": $contents}');
          return;
        } else if (value.statusCode >= 500) {
          completer.complete('{"statusCode": ${value.statusCode}, "error": ${jsonEncode(contents)}}');
          return;
        }
        completer.complete(contents);
      });
      return completer.future;
    }, onError: (error) {
      consoleBus.error(error);
      return 'ERROR';
    });
  }

  static Future<String> runTest(dynamic data) async {
    final String hostUrl = getStringSetting(URL_KEY, URL_KEY_DEFAULT_VALUE);
    String url = '$hostUrl/api/test/run/';

    final httpClient = HttpClient();
    final request = await httpClient.openUrl('POST', Uri.parse(url));
    request.headers.add('Content-type', 'application/json');
    request.headers.add('Accept', 'application/json');
    request.headers.add('Authorization', 'Basic YWRtaW46cG9zdGdyZXM=');
    var result = jsonEncode(data);
    request.write(result);
    final response = request.close();
    return response.then((value) {
      var completer = Completer<String>();
      value.transform(utf8.decoder).listen((contents) {
        if (value.statusCode < 200 || (value.statusCode >= 300 && value.statusCode < 500)) {
          completer.complete('{"statusCode": ${value.statusCode}, "error": $contents}');
          return;
        } else if (value.statusCode >= 500) {
          completer.complete('{"statusCode": ${value.statusCode}, "error": ${jsonEncode(contents)}}');
          return;
        }
        completer.complete(contents);
      });
      return completer.future;
    }, onError: (error) {
      consoleBus.error(error);
      return 'ERROR';
    });
  }
}
