import 'dart:async';
import 'dart:convert';

import 'package:bhfront/events/buses/console_bus.dart';
import 'package:bhfront/utils/constants.dart';
import 'package:bhfront/utils/profile.dart';
import 'package:universal_io/prefer_universal/io.dart';

class TestStageService {
  static final ConsoleBus consoleBus = ConsoleBus();

  static Future<String> testStages(String mnemonic) async {
    final String hostUrl = getStringSetting(URL_KEY, URL_KEY_DEFAULT_VALUE);
    String url = '$hostUrl/api/test-stage-history/last/$mnemonic/';

    final httpClient = HttpClient();
    final request = await httpClient.openUrl('GET', Uri.parse(url));
    request.headers.add('Content-type', 'application/json');
    request.headers.add('Accept', 'application/json');
    request.headers.add('Authorization', 'Basic YWRtaW46cG9zdGdyZXM=');
    final response = request.close();
    return response.then((value) {
      var completer = Completer<String>();
      value.transform(utf8.decoder).listen((contents) {
        completer.complete(contents);
      });
      return completer.future;
    });
  }

}
