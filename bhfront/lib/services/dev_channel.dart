import 'dart:async';

import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/status.dart' as status;

class DevChannel {

  static Future<String> lastRecords(WebSocketChannel channel) async {
    var completer = Completer<String>();
    channel.stream.listen((message) {
      completer.complete(message);
      channel.sink.add("received!");
      channel.sink.close(status.goingAway);
    });
    return completer.future;
  }
}