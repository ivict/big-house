enum LogLevel {
  debug,
  info,
  error,
}

logLevelToString(LogLevel logLevel) {
  switch (logLevel) {
    case LogLevel.debug:
      return 'DEBUG';
    case LogLevel.info:
      return 'INFO';
    case LogLevel.error:
      return 'ERROR';
    default:
      throw ArgumentError('Not implemented for $logLevel');
  }
}

class ConsoleEvent {
  final dynamic data;
  final LogLevel logLevel;
  final String emitter;

  ConsoleEvent(this.data, this.logLevel, this.emitter);
}
