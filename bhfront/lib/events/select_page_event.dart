class SelectPageEvent {
  final int index;
  final dynamic data;

  SelectPageEvent(this.index, {this.data});

}