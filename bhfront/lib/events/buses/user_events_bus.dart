import 'dart:async';

import 'package:event_bus/event_bus.dart';

class UserEventsBus {
  final EventBus eventBus = EventBus();
  static final UserEventsBus _singleton = UserEventsBus._internal();

  factory UserEventsBus() {
    return _singleton;
  }

  UserEventsBus._internal();

  push(dynamic event) {
    eventBus.fire(event);
  }

  StreamSubscription subscribe(void onData(event),
      {Function onError, void onDone(), bool cancelOnError, Type eventType}) {
    if (eventType != null) {
      return eventBus.on().listen((event) {
        // EVENT FILTER
        if (event.runtimeType != eventType) {
          return;
        }
        onData(event);
      }, onError: onError, onDone: onDone, cancelOnError: cancelOnError);
    }
    return eventBus.on().listen(onData,
        onError: onError, onDone: onDone, cancelOnError: cancelOnError);
  }

}
