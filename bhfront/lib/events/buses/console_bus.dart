import 'dart:math';

import 'package:bhfront/events/console_event.dart';
import 'package:event_bus/event_bus.dart';

class ConsoleBus {
  final EventBus eventBus = EventBus();
  static final ConsoleBus _singleton = ConsoleBus._internal();
  static final RegExp _filePositionInfoExp = RegExp(r'(\d+\:\d+)');
  static final bool _enableFilePosition = true;

  factory ConsoleBus() {
    return _singleton;
  }

  ConsoleBus._internal();

  log(LogLevel logLevel, dynamic event) {
    l(logLevel, _lookupEmitter(_enableFilePosition), event);
  }

  l(LogLevel logLevel, dynamic emitter, dynamic event) {
    switch (logLevel) {
      case LogLevel.debug:
        eventBus.fire(ConsoleEvent(event, LogLevel.debug, emitter.toString()));
        break;
      case LogLevel.info:
        eventBus.fire(ConsoleEvent(event, LogLevel.info, emitter.toString()));
        break;
      case LogLevel.error:
        eventBus.fire(ConsoleEvent(event, LogLevel.error, emitter.toString()));
        break;
      default:
        throw ArgumentError('Not implemented for $logLevel');
    }
  }

  debug(dynamic event) {
    d(_lookupEmitter(_enableFilePosition), event);
  }

  d(dynamic emitter, dynamic event) {
    eventBus.fire(ConsoleEvent(event, LogLevel.debug, emitter.toString()));
  }

  info(dynamic event) {
    i(_lookupEmitter(_enableFilePosition), event);
  }

  i(dynamic emitter, dynamic event) {
    eventBus.fire(ConsoleEvent(event, LogLevel.info, emitter.toString()));
  }

  error(dynamic event) {
    e(_lookupEmitter(_enableFilePosition), event);
  }

  e(dynamic emitter, dynamic event) {
    eventBus.fire(ConsoleEvent(event, LogLevel.error, emitter.toString()));
  }

  subscribe(void onData(ConsoleEvent consoleEvent),
      {Function onError, void onDone(), bool cancelOnError}) {
    eventBus.on<ConsoleEvent>().listen(onData);
  }

  String _lookupEmitter(bool includeLineInfo) {
    try {
      throw 0;
    } catch (_, stack) {
      var stackAsText = stack.toString();
      int start = stackAsText.indexOf(
          '\n', stackAsText.indexOf('\n', stackAsText.indexOf('\n') + 1) + 1);
      int end = stackAsText.indexOf('\n', start + 1);
      String markedLine = stackAsText.substring(start + 1, end);
      var match = _filePositionInfoExp.firstMatch(markedLine);
      if (match == null) {
        return markedLine;
      }
      if (includeLineInfo) {
        return markedLine.substring(0, match.end) +
            ' ' +
            markedLine.substring(match.end, markedLine.length).trimLeft();
      }
      return markedLine.substring(match.end, markedLine.length).trimLeft();
    }
  }
}


final ConsoleBus consoleBus = ConsoleBus();
