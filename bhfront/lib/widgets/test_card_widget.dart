import 'dart:async';
import 'dart:convert';

import 'package:bhfront/events/buses/console_bus.dart';
import 'package:bhfront/events/buses/user_events_bus.dart';
import 'package:bhfront/events/console_event.dart';
import 'package:bhfront/events/select_page_event.dart';
import 'package:bhfront/events/user_select_test_event.dart';
import 'package:bhfront/services/test_service.dart';
import 'package:bhfront/services/test_stages_service.dart';
import 'package:bhfront/utils/constants.dart';
import 'package:bhfront/utils/styles.dart';
import 'package:bhfront/utils/tools.dart';
import 'package:bhfront/widgets/test_run_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum TestCardState { NOT_SELECTED, SELECTED_USUAL, SELECTED_IN_PROGRESS }

extension TestCardStateExtension on TestCardState {
  TestCardState inverted(bool inProgress) {
    switch (this) {
      case TestCardState.NOT_SELECTED:
        return inProgress
            ? TestCardState.SELECTED_IN_PROGRESS
            : TestCardState.SELECTED_USUAL;
      case TestCardState.SELECTED_USUAL:
        return TestCardState.NOT_SELECTED;
      case TestCardState.SELECTED_IN_PROGRESS:
        return TestCardState.NOT_SELECTED;
      default:
        throw ArgumentError('Not implemented case for: ${this}');
    }
  }
}

class TestCardWidget extends StatefulWidget {
  final int index;
  final dynamic data;
  final bool longPressEnabled;
  final VoidCallback callback;

  // INITIAL STATE OF SELECTION
  final TestCardState selected;

  const TestCardWidget(
      {Key key,
      this.index,
      this.data,
      this.longPressEnabled,
      this.callback,
      this.selected})
      : super(key: key);

  @override
  _TestCardWidgetState createState() =>
      _TestCardWidgetState(selected: selected);
}

class _TestCardWidgetState extends State<TestCardWidget> {
  final UserEventsBus userEventBus = UserEventsBus();

  TestCardState selected = TestCardState.NOT_SELECTED;

  StreamSubscription _userEventBusSubscription;

  List<dynamic> stages;

  _TestCardWidgetState({this.selected});

  @override
  void initState() {
    super.initState();
    _userEventBusSubscription = userEventBus.subscribe((selectTestEvent) {
      if (selectTestEvent.index != widget.index &&
          selected != TestCardState.NOT_SELECTED) {
        setState(() => selected = TestCardState.NOT_SELECTED);
      }
    }, eventType: UserSelectTestEvent);
  }

  @override
  void dispose() {
    _userEventBusSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () {
        setState(() {
          selected = selected.inverted(check(widget.data, 'in_progress'));
        });
        widget.callback();
      },
      onTap: () {
        if (widget.longPressEnabled) {
          setState(() {
            selected = selected.inverted(check(widget.data, 'in_progress'));
          });
          widget.callback();
        }
      },
      child: Container(
        margin: EdgeInsets.all(2.0),
        child: Stack(
          children: <Widget>[
            Align(
                alignment: Alignment.topLeft,
                child: Card(
                  elevation: 0.1,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0))),
                  child: _cardContent(selected),
                )),
            Align(
              alignment: Alignment.topRight,
              child: Text('Long tap for test selection',
                  style: TextStyle(color: Colors.grey, fontSize: 8.0)),
            )
          ],
        ),
        decoration: selected != TestCardState.NOT_SELECTED
            ? BoxDecoration(
                color: Colors.green,
                borderRadius: BorderRadius.all(Radius.circular(5.0)))
            : BoxDecoration(),
      ),
    );
  }

  Widget _cardContent(TestCardState selected) {
    switch (selected) {
      case TestCardState.NOT_SELECTED:
        return ListTile(
          title: _headerOfCard(selected),
          subtitle: Text(widget.data['content'].toString(), maxLines: 1),
        );
      case TestCardState.SELECTED_USUAL:
        return ListTile(
            contentPadding: EdgeInsets.only(
                left: 16.0, right: 16.0, bottom: 10.0, top: 8.0),
            title: _headerOfCard(selected),
            subtitle: Row(
              children: <Widget>[
                Expanded(
                  child: RichText(
                      text: TextSpan(
                    text: widget.data['summary'],
                    style: TEST_CARD_SUMMARY_STYLE,
                  )),
                ),
                _buttonsPanel(selected)
              ],
            ));
      case TestCardState.SELECTED_IN_PROGRESS:
        return ListTile(
            contentPadding: EdgeInsets.only(
                left: 16.0, right: 16.0, bottom: 10.0, top: 8.0),
            title: _headerOfCard(selected),
            subtitle: Row(
              children: <Widget>[
                Expanded(
                    child: Padding(
                  padding: EdgeInsets.only(left: 40.0, right: 40.0, top: 8.0),
                  child: _progressTable(stages),
                )),
                _buttonsPanel(selected)
              ],
            ));
      default:
        throw ArgumentError('Unimplemented case: $selected');
    }
    // EXPANDED CARD [FULL VIEW]
  }

  List<TableRow> _tableRows(List<dynamic> stages) {
    if (stages == null) {
      return [];
    }
    return stages.map((stage) {
      return TableRow(children: [
        Container(
          alignment: Alignment.centerRight,
          child: Text(stage['stage_name'],
              style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.w500)),
          padding: EdgeInsets.all(4.0),
        ),
        Container(
          alignment: Alignment.centerLeft,
          child: Text(_notNullString(stage['stage_status_name'], ''),
              style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.w600)),
          padding: EdgeInsets.all(4.0),
        ),
      ]);
    }).toList();
  }

  RichText _headerOfCard(TestCardState selected) {
    return RichText(
      text: TextSpan(
          text: widget.data['mnemonic'],
          style: TEST_MNEMONIC_STYLE,
          children: <TextSpan>[
            TextSpan(
                text:
                    '\t' + widget.data['name'] + _status(selected, widget.data),
                style: TEST_NAME_STYLE)
          ]),
    );
  }

  String _status(TestCardState selected, data) {
    if (selected == TestCardState.NOT_SELECTED && check(data, 'in_progress')) {
      return ' [in progress]';
    }
    return '';
  }

  Widget _buttonsPanel(TestCardState selected) {
    const padding = EdgeInsets.all(2.0);
    const singleButtonConstraints = BoxConstraints.tightFor(
        width: RUN_BUTTON_SIZE, height: RUN_BUTTON_SIZE);
    return Container(
        padding: padding,
        decoration: BoxDecoration(
          color: Colors.green,
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                    padding: padding,
                    child: ConstrainedBox(
                      constraints: singleButtonConstraints,
                      child: _testRunButton(selected, widget.data),
                    )),
                Padding(
                    padding: padding,
                    child: ConstrainedBox(
                        constraints: singleButtonConstraints,
                        child: ClipOval(
                            child: Container(
                          color: Colors.black12,
                          height: RUN_BUTTON_SIZE,
                          width: RUN_BUTTON_SIZE,
                        )))),
              ],
            ),
            Row(
              children: <Widget>[
                Padding(
                    padding: padding,
                    child: ConstrainedBox(
                        constraints: singleButtonConstraints,
                        child: ClipOval(
                            child: Container(
                          color: Colors.black12,
                          height: RUN_BUTTON_SIZE,
                          width: RUN_BUTTON_SIZE,
                        )))),
                Padding(
                    padding: padding,
                    child: ConstrainedBox(
                        constraints: singleButtonConstraints,
                        child: ClipOval(
                          child: Container(
                            color: Colors.black26,
                            height: RUN_BUTTON_SIZE,
                            width: RUN_BUTTON_SIZE,
                            child: _testEditButton(selected, widget.data),
                          ),
                        )))
              ],
            ),
          ],
        ));
  }

  Widget _testRunButton(selected, data) {
    if (selected == TestCardState.SELECTED_IN_PROGRESS) {
      return new CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
      );
    }
    return TestRunButton(
      enabled: true,
      onPressed: () async {
        String responseBody = await TestService.runTest(data);
        return () async {
          dynamic decodedResponse = jsonDecode(responseBody);
          consoleBus.log(
              decodedResponse['error'] != null
                  ? LogLevel.error
                  : LogLevel.debug,
              'RESPONSE $decodedResponse');
          if (decodedResponse['error'] == null) {
            // HACK
            widget.data['in_progress'] = true;
            // Show test progress details
            await _requestStages(data);
          }
        };
      },
    );
  }

  Future _requestStages(data) async {
    String responseBody = await TestStageService.testStages(data['mnemonic']);
    _parseStagesAndSubscribeToUpdates(responseBody);
    setState(() => selected = TestCardState.SELECTED_IN_PROGRESS);
  }

  _parseStagesAndSubscribeToUpdates(String responseBody) {
    var parsedStages = jsonDecode(responseBody);
    // TODO: Subscribe to update run info
    stages = parsedStages;
  }

  Widget _testEditButton(selected, data) {
    if (selected == TestCardState.SELECTED_IN_PROGRESS) {
      return Icon(
        Icons.edit,
        size: 25.0,
        color: Colors.grey,
      );
    }
    return IconButton(
      icon: Icon(
        Icons.edit,
        size: 25.0,
        color: Colors.white,
      ),
      onPressed: () {
        consoleBus.d(this, 'EDIT TEST');
        userEventBus.push(SelectPageEvent(1, data: data));
      },
    );
  }

  String _notNullString(String string, String defaultValue) {
    return string != null ? string : defaultValue;
  }

  Widget _progressTable(List stages) {
    if (stages == null) {
      return FutureBuilder(
        future: TestStageService.testStages(widget.data['mnemonic']),
        builder: (context, snapshot) {
          _parseStagesAndSubscribeToUpdates(snapshot.data.toString());
          consoleBus.debug(this.stages);
          return Table(
            // defaultColumnWidth: FixedColumnWidth(150.0),
            columnWidths: <int, TableColumnWidth>{1: FixedColumnWidth(70)},
            border: TableBorder(
              horizontalInside: BorderSide(
                color: Colors.grey,
                style: BorderStyle.solid,
                width: 0.2,
              ),
              verticalInside: BorderSide(
                color: Colors.grey,
                style: BorderStyle.solid,
                width: 0.2,
              ),
            ),
            children: _tableRows(this.stages),
          );
        },
      );
    }
    return Table(
      // defaultColumnWidth: FixedColumnWidth(150.0),
      columnWidths: <int, TableColumnWidth>{1: FixedColumnWidth(70)},
      border: TableBorder(
        horizontalInside: BorderSide(
          color: Colors.grey,
          style: BorderStyle.solid,
          width: 0.2,
        ),
        verticalInside: BorderSide(
          color: Colors.grey,
          style: BorderStyle.solid,
          width: 0.2,
        ),
      ),
      children: _tableRows(stages),
    );
  }
}
