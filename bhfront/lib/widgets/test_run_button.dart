import 'package:bhfront/utils/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_progress_button/flutter_progress_button.dart';

class TestRunButton extends ProgressButton {
  final bool enabled;

  TestRunButton({
    Key key,
    this.enabled: true,
    Function onPressed,
  }) : super(
          key: key,
          borderRadius: 20.0,
          color: enabled ? Colors.black26 : Colors.black12,
          onPressed: enabled ? onPressed : null,
          defaultWidget: enabled
              ? Icon(
                  Icons.play_arrow,
                  size: 30.0,
                  semanticLabel: 'Run test',
                  color: Colors.white,
                  // color: Colors.grey,
                )
              : Icon(
                  Icons.play_arrow,
                  size: 30.0,
                  semanticLabel: 'Run test',
                  color: Colors.grey,
                ),
          progressWidget: const CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white)),
          width: RUN_BUTTON_SIZE,
          height: RUN_BUTTON_SIZE,
        );
}
