import 'package:flutter/cupertino.dart';

class LogConsoleWidget extends RichText {

  LogConsoleWidget({
    Key key,
    text,
    textAlign,
    textDirection,
    softWrap,
    overflow,
    textScaleFactor,
    maxLines,
    locale,
    strutStyle,
    textWidthBasis,
  }) : super(
    key: key,
    text: text,
    textAlign: textAlign,
    textDirection: textDirection,
    softWrap: softWrap,
    overflow: overflow,
    textScaleFactor: textScaleFactor,
    maxLines: maxLines,
    locale: locale,
    strutStyle: strutStyle,
    textWidthBasis: textWidthBasis,
  );
}
