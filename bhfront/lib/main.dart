import 'package:bhfront/pages/main_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(BhFront());

class BhFront extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Big house (clever tools)',
      theme: ThemeData(
          // Define application theme
          fontFamily: 'Montserrat',
          primarySwatch: Colors.green,
          brightness: Brightness.light,
          textTheme: TextTheme(
            subhead: TextStyle(fontWeight: FontWeight.w400, fontSize: 17),
          )),
      home: MainPage(),
    );
  }
}
