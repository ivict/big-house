library constants;

// PROFILE:
const String URL_KEY = 'server.url';
const String URL_KEY_DEFAULT_VALUE = 'http://localhost:8000';

// EDIT PAGE
const String TEST_EDIT_PAGE_CONTENT_KEY = 'text.edit.page.content';
const String TEST_EDIT_PAGE_NAME_KEY = 'text.edit.page.name';
const TEST_EDIT_PAGE_MNEMONIC_KEY = 'test.edit.page.mnemonic';

const String TEST_EDIT_PAGE_CONTENT_DEFAULT_VALUE = '''
docker image: _
cluster nodes: 2
replication: 2

data-model:
 	create table1 (id int, name varchar(255));
 
initial-data:
        insert into table1 values (random-data-10000);

scenario:
        select * from table1 by row basis;
''';


// USER HISTORY:
const String SELECTED_DRAWER_INDEX = 'selected.drawer.index';
const int SELECTED_DRAWER_INDEX_DEFAULT_VALUE = 0;

// RUN BUTTON
const double RUN_BUTTON_SIZE = 42;

// PROFILE
const String PROFILE_ABOUT_TEXT = '''
Performance testing is important part of any product development.
But it's not easy. Inspired by this dificult situation, developers
of that  tool, help testers resolve this issue. This tool have
unique possibility for tester, write test scenario, that can 
crashed any backend.
''';

