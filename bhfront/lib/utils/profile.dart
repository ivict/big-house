library profile;

import 'dart:html';

import 'package:bhfront/events/buses/console_bus.dart';

int getIntSetting(String key, int defaultValue) {
  try {
    var valueFromLocalStorage = window.localStorage[key];
    consoleBus.debug('VALUE [$key] FROM LOCAL STORAGE: $valueFromLocalStorage');
    return int.parse(valueFromLocalStorage ?? defaultValue.toString());
  } on Exception catch (_) {
    consoleBus.debug(
        'VALUE [$key] FROM LOCAL STORAGE [DEFAULT ON ERROR]: $defaultValue');
    return defaultValue;
  }
}

String getStringSetting(String key, String defaultValue) {
  var valueFromLocalStorage = window.localStorage[key];
  consoleBus.debug('VALUE [$key] FROM LOCAL STORAGE: $valueFromLocalStorage');
  return valueFromLocalStorage ?? defaultValue;
}

void setIntSetting(String key, int value) {
  consoleBus.debug('VALUE [$key] PUSH TO LOCAL STORAGE: $value');
  window.localStorage[key] = value.toString();
}

void setStringSetting(String key, String value) {
  consoleBus.debug('VALUE [$key] PUSH TO LOCAL STORAGE: $value');
  if (value == null) {
    window.localStorage.remove(key);
    return;
  }
  window.localStorage[key] = value;
}
