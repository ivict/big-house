library tools;

import 'package:bhfront/events/buses/console_bus.dart';

bool check(dynamic data, String boolKey) {
  consoleBus.debug('DATA BOOL KEY : ${data[boolKey]}');
  return data != null && data[boolKey] != null && data[boolKey];
}
