library styles;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const TextStyle TEST_MNEMONIC_STYLE = TextStyle(
    fontFamily: 'Montserrat',
    fontWeight: FontWeight.w800,
    decorationThickness: 2.0,
    // backgroundColor: Colors.white,
    color: Colors.black87,
    fontSize: 14.0);

const TextStyle TEST_NAME_STYLE = TextStyle(
    fontFamily: 'Montserrat', fontWeight: FontWeight.w400, fontSize: 14.0);

const TextStyle TEST_CONTENT_STYLE = TextStyle(
    letterSpacing: 1.0,
    // decorationThickness: 2.0,
    fontFamily: 'Source Code Pro',
    backgroundColor: Colors.white,
    fontSize: 16.0,
    color: Colors.black,
    fontWeight: FontWeight.normal);

const TextStyle PROFILE_ABOUT_STYLE = TextStyle(
    fontFamily: 'Montserrat',
    fontWeight: FontWeight.w300,
    decorationThickness: 1.0,
    color: Colors.blueGrey,
    fontSize: 14.0);

const TextStyle TEST_CARD_SUMMARY_STYLE = TextStyle(
    fontFamily: 'Montserrat',
    fontWeight: FontWeight.w300,
    decorationThickness: 1.0,
    color: Colors.black,
    fontSize: 14.0);
