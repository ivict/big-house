import 'dart:convert';

import 'package:bhfront/events/buses/console_bus.dart';
import 'package:bhfront/events/buses/user_events_bus.dart';
import 'package:bhfront/events/user_select_test_event.dart';
import 'package:bhfront/services/test_service.dart';
import 'package:bhfront/utils/tools.dart';
import 'package:bhfront/widgets/test_card_widget.dart';
import 'package:flutter/material.dart';

class TestListPage extends StatefulWidget {
  TestListPage({Key key, this.title, this.behaviourData}) : super(key: key);

  final String title;
  final dynamic behaviourData;

  @override
  _TestListPageState createState() => _TestListPageState(
      behaviourData != null ? behaviourData['searchQuery'] : '',
      behaviourData != null && behaviourData['auto'] ? 0 : -1);
}

class _TestListPageState extends State<TestListPage> {
  final UserEventsBus userEventsBus = UserEventsBus();

  String searchQuery;
  bool longPressFlag = false;
  List<dynamic> searchResults = [];

  int selectedIndex;

  final TextEditingController _searchTestController = TextEditingController();

  _TestListPageState(this.searchQuery, this.selectedIndex);

  void longPress() {}

  searchTests(query) async {
    TestService.searchTests(query).then((responseBody) {
      var paginatedData = jsonDecode(responseBody);
      List<dynamic> data = paginatedData['results'];
      setState(() {
        data.forEach((value) {
          // Adding search results
          searchResults.add(value);
        });
      });
    });
  }

  @override
  void initState() {
    super.initState();
    consoleBus.debug('search query: $searchQuery');
    _searchTestController.text = searchQuery == null ? '' : searchQuery;
    searchTests(searchQuery == null ? '' : searchQuery);
    consoleBus.debug('LIST INDEX: $selectedIndex');
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called
    return ListView(children: <Widget>[
      Padding(
        padding: const EdgeInsets.all(10.0),
        child: TextField(
          controller: _searchTestController,
          onChanged: (val) {
            // Clear cached results
            searchResults.clear();

            // Call search query
            searchTests(val);
          },
          decoration: InputDecoration(
              contentPadding: EdgeInsets.only(left: 25.0),
              hintText: 'Test name or mnemonic',
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(4.0)),
              suffixIcon: IconButton(
                icon: Icon(Icons.search),
                onPressed: null,
              )),
        ),
      ),
      SizedBox(
        height: 10.0,
      ),
      ListView.builder(
        shrinkWrap: true,
        itemCount: searchResults.length,
        itemBuilder: (BuildContext context, int index) {
          return TestCardWidget(
            selected: _testCardState(index),
            index: index,
            data: searchResults[index],
            longPressEnabled: longPressFlag,
            callback: () {
              longPress();
              if (selectedIndex != index) {
                // RESET SELECTED FLAG FROM NOT SELECTED ITEMS
                userEventsBus.push(UserSelectTestEvent(index));
                selectedIndex = index;
              }
              longPressFlag = false;
            },
          );
        },
      )
    ]);
  }

  TestCardState _testCardState(int index) {
    if (selectedIndex != index) {
      return TestCardState.NOT_SELECTED;
    }
    dynamic data = searchResults[index];
    if (data == null) {
      return TestCardState.NOT_SELECTED;
    }
    if (check(data, 'in_progress')) {
      return TestCardState.SELECTED_IN_PROGRESS;
    }
    return TestCardState.SELECTED_USUAL;
  }
}
