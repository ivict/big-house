import 'dart:collection';
import 'dart:math';

import 'package:bhfront/events/buses/console_bus.dart';
import 'package:bhfront/events/user_change_test_event.dart';
import 'package:bhfront/events/buses/user_events_bus.dart';
import 'package:bhfront/utils/constants.dart';
import 'package:bhfront/utils/profile.dart';
import 'package:bhfront/utils/styles.dart';
import 'package:bhfront/widgets/test_script_editor.dart';
import 'package:flutter/material.dart';

resetTestTemporaryStorage() {
  setStringSetting(TEST_EDIT_PAGE_NAME_KEY, null);
  setStringSetting(TEST_EDIT_PAGE_MNEMONIC_KEY, null);
  setStringSetting(TEST_EDIT_PAGE_CONTENT_KEY, null);
}

class TestEditPage extends StatefulWidget {
  TestEditPage({Key key, this.title, this.test}) : super(key: key);

  final String title;
  final dynamic test;

  @override
  _TestEditPageState createState() => _TestEditPageState(test: test);
}

class _TestEditPageState extends State<TestEditPage> {
  UserEventsBus userEventsBus = UserEventsBus();
  dynamic test;

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _mnemonicController = TextEditingController();
  final TextEditingController _contentController = TextEditingController();
  final ScrollController _contentScrollController = ScrollController();
  final ScrollPhysics _contentScrollPhysics = ScrollPhysics();

  _TestEditPageState({this.test});

  @override
  void initState() {
    super.initState();
    if (test == null) {
      test = LinkedHashMap<String, dynamic>();
      test['name'] = getStringSetting(TEST_EDIT_PAGE_NAME_KEY, '');
      test['content'] = getStringSetting(
          TEST_EDIT_PAGE_CONTENT_KEY, TEST_EDIT_PAGE_CONTENT_DEFAULT_VALUE);
      test['mnemonic'] = getStringSetting(TEST_EDIT_PAGE_MNEMONIC_KEY,
          'test' + Random().nextInt(1000000).toString());
      // PREVENT TO UNEXPECTED CHANGE OF THE VALUE
      setStringSetting(TEST_EDIT_PAGE_MNEMONIC_KEY, test['mnemonic']);
    }
    _nameController.text = test['name'];
    _mnemonicController.text = test['mnemonic'];
    _contentController.text = test['content'];
    test['initial_mnemonic'] = test['mnemonic'];
    // UPDATE STATE FOR RUN BUTTON
    _pushEventOnValidTest(test);
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called
    return Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(children: <Widget>[
          TextField(
            style: TEST_MNEMONIC_STYLE,
            decoration: const InputDecoration(
              hintText: 'Unique name',
              labelText: 'Mnemonic',
            ),
            autocorrect: false,
            controller: _mnemonicController,
            onChanged: (text) {
              test['mnemonic'] = text;
              setStringSetting(
                  TEST_EDIT_PAGE_MNEMONIC_KEY, text.isEmpty ? null : text);
              _pushEventOnValidTest(test);
            },
          ),
          TextField(
            style: TEST_NAME_STYLE,
            decoration: const InputDecoration(
              hintText: 'Test distingushable name',
              labelText: 'Name',
            ),
            autocorrect: false,
            controller: _nameController,
            onChanged: (text) {
              test['name'] = text;
              setStringSetting(
                  TEST_EDIT_PAGE_NAME_KEY, text.isEmpty ? null : text);
              _pushEventOnValidTest(test);
            },
          ),
          Expanded(
            child: TestScriptEditorWidget(
              decoration: const InputDecoration(
                labelText: 'Scenario',
              ),
              scrollController: _contentScrollController,
              scrollPhysics: _contentScrollPhysics,
              controller: _contentController,
              onChanged: (value) {
                test['content'] = value;
                setStringSetting(
                    TEST_EDIT_PAGE_CONTENT_KEY, value.isEmpty ? null : value);
                _pushEventOnValidTest(test);
              },
            ),
          )
        ]));
  }

  void _pushEventOnValidTest(test) {
    if (_isValidTest(test)) {
      userEventsBus.push(UserChangeTestEvent(test));
    } else {
      // DISABLE RUN BUTTON
      userEventsBus.push(UserChangeTestEvent(null));
    }
  }

  bool _isValidTest(test) {
    var valid = test['content'] != null &&
        (test['content'] as String).isNotEmpty &&
        test['mnemonic'] != null &&
        (test['mnemonic'] as String).isNotEmpty &&
        test['name'] != null &&
        (test['name'] as String).isNotEmpty;
    if (!valid) {
      consoleBus.debug('test not valid, reason: ${_invalidReason(test)}');
    }
    return valid;
  }

  String _invalidReason(test) {
    StringBuffer reason = StringBuffer();
    if (test['content'] == null) {
      reason.write(' content is null');
    } else if ((test['content'] as String).isEmpty) {
      reason.write(' content is empty');
    }
    if (test['mnemonic'] == null) {
      reason.write(' mnemonic is null');
    } else if ((test['mnemonic'] as String).isEmpty) {
      reason.write(' mnemonic is empty');
    }
    if (test['name'] == null) {
      reason.write(' name is null');
    } else if ((test['name'] as String).isEmpty) {
      reason.write(' name is empty');
    }
    return reason.toString();
  }
}
