import 'dart:async';
import 'dart:collection';
import 'dart:convert';

import 'package:bhfront/events/buses/console_bus.dart';
import 'package:bhfront/events/buses/user_events_bus.dart';
import 'package:bhfront/events/console_event.dart';
import 'package:bhfront/events/select_page_event.dart';
import 'package:bhfront/events/user_change_test_event.dart';
import 'package:bhfront/pages/log_page.dart';
import 'package:bhfront/pages/profile_page.dart';
import 'package:bhfront/pages/test_edit_page.dart';
import 'package:bhfront/pages/test_list_page.dart';
import 'package:bhfront/services/test_service.dart';
import 'package:bhfront/utils/constants.dart';
import 'package:bhfront/utils/profile.dart';
import 'package:bhfront/widgets/test_run_button.dart';
import 'package:flutter/material.dart';

enum ActionButton { testRun }

class DrawerItem {
  final String title;
  final IconData icon;
  final List<ActionButton> actionButtons;
  final String Function(dynamic data) pageTitle;

  DrawerItem(this.title, this.icon, this.actionButtons, {this.pageTitle});

  String getTitle(dynamic data) {
    if (pageTitle != null) {
      return pageTitle(data);
    }
    return title.toUpperCase();
  }
}

class MainPage extends StatefulWidget {
  final drawerItems = [
    DrawerItem("Test list", Icons.list, []),
    DrawerItem("New test", Icons.open_in_new, [ActionButton.testRun],
        pageTitle: (data) {
      return data != null ? 'TEST: ${data["name"]}' : 'NEW TEST';
    }),
    DrawerItem("Dev channel", Icons.message, []),
    DrawerItem("Profile", Icons.settings, []),
  ];

  MainPage({Key key}) : super(key: key);

  // This widget is the home page.
  // This class is the configuration for the state.
  // Fields in a Widget subclass are
  // always marked "final".

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final UserEventsBus userEventsBus = UserEventsBus();
  final ConsoleBus consoleBus = ConsoleBus();

  int _selectedDrawerIndex =
      getIntSetting(SELECTED_DRAWER_INDEX, SELECTED_DRAWER_INDEX_DEFAULT_VALUE);
  dynamic _data;

  StreamSubscription _userEventBusSubscription;
  StreamSubscription _consoleBusSubscription;

  // DATA CHANGED AFTER USER INPUT ON TEST-EDIT-PAGE
  dynamic _testForSaving;

  _getDrawerItemWidget(int position) {
    consoleBus.debug('position: $position, _data: $_data ');
    switch (position) {
      case 0:
        return TestListPage(behaviourData: _data);
      case 1:
        return TestEditPage(test: _data);
      case 2:
        return LogPage();
      case 3:
        return ProfilePage();

      default:
        return Text("Error: undefined page");
    }
  }

  _onSelectItem(int index, {bool auto = false, dynamic data}) {
    setState(() {
      _selectedDrawerIndex = index;
      _data = data;
    });
    setIntSetting(SELECTED_DRAWER_INDEX, index);
    if (auto) {
      return;
    }
    Navigator.of(context).pop(); // close the drawer
  }

  @override
  void initState() {
    super.initState();
    _userEventBusSubscription = userEventsBus.subscribe((event) {
      // SIMPLE ROUTER
      if (event is UserChangeTestEvent) {
        setState(() => _testForSaving = event.test);
      } else if (event is SelectPageEvent) {
        _onSelectItem(event.index, auto: true, data: event.data);
      }
    });
    _consoleBusSubscription = consoleBus.subscribe((consoleEvent) {
      print(
          '[${DateTime.now()}][${logLevelToString(consoleEvent.logLevel)}] [${consoleEvent.emitter}] ${consoleEvent.data}');
    });
  }

  @override
  void dispose() {
    _userEventBusSubscription.cancel();
    _consoleBusSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called
    var drawerOptions = <Widget>[];
    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(ListTile(
        leading: Icon(d.icon),
        title: Text(d.title),
        selected: i == _selectedDrawerIndex,
        onTap: () => _onSelectItem(i),
      ));
    }
    MediaQueryData queryData = MediaQuery.of(context);
    double maxWidth =
        queryData.size.width > 640 ? 570 : queryData.size.width - 40;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          title: ConstrainedBox(
              constraints: BoxConstraints(
                  minWidth: 50,
                  minHeight: 40,
                  maxHeight: 40,
                  maxWidth: maxWidth),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: _selectedDrawerWidgets(
                      widget.drawerItems[_selectedDrawerIndex]))),
          actions: <Widget>[],
        ),
        drawer: Drawer(
          child: Column(
            children: <Widget>[
              UserAccountsDrawerHeader(
                  accountName: Text("John Doe"), accountEmail: null),
              Column(children: drawerOptions)
            ],
          ),
        ),
        body: _wideScreensExtraAdapted(context, _selectedDrawerIndex));
  }

  _wideScreensExtraAdapted(BuildContext context, int position) {
    MediaQueryData queryData = MediaQuery.of(context);
    if (queryData.size.width > 640 + 10) {
      return Row(children: <Widget>[
        Container(
          constraints: BoxConstraints(minWidth: 320, maxWidth: 640),
          child: _getDrawerItemWidget(_selectedDrawerIndex),
        ),
        Expanded(
          child: Container(
            decoration: BoxDecoration(color: Colors.black12),
            child: null /* add child content here */,
          ),
        )
      ]);
    }
    return Container(
      constraints: BoxConstraints(minWidth: 320, maxWidth: 640),
      child: _getDrawerItemWidget(_selectedDrawerIndex),
    );
  }

  _selectedDrawerWidgets(DrawerItem drawerItem) {
    List<Widget> result = <Widget>[
      Text(
        drawerItem.getTitle(_data),
        style: TextStyle(
            fontWeight: FontWeight.w900, fontSize: 15.0, letterSpacing: 1.2),
      ),
      Spacer()
    ];
    if (drawerItem.actionButtons.contains(ActionButton.testRun)) {
      result.add(TestRunButton(
        enabled: _testForSaving != null,
        onPressed: () async {
          String responseBody = await TestService.saveTest(_testForSaving);
          return () {
            dynamic data = jsonDecode(responseBody);
            consoleBus.log(
                data['error'] != null ? LogLevel.error : LogLevel.debug,
                'RESPONSE $data');
            if (data['error'] == null) {
              // Clear temporary storage
              resetTestTemporaryStorage();
              var searchQuery = _testForSaving['mnemonic'];
              _testForSaving = null; // DISABLE RUN BUTTON
              var behaviourData = HashMap<String, dynamic>();
              behaviourData['searchQuery'] = searchQuery;
              behaviourData['auto'] = true;
              _onSelectItem(0, auto: true, data: behaviourData);
            }
          };
        },
      ));
    }
    return result;
  }
}
