import 'package:bhfront/utils/constants.dart';
import 'package:bhfront/utils/profile.dart';
import 'package:bhfront/utils/styles.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final TextEditingController _urlController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _urlController.text = getStringSetting(URL_KEY, '');
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called
    return Column(children: <Widget>[
      Flexible(
        child: TextField(
          decoration: const InputDecoration(
            icon: Icon(Icons.settings_ethernet),
            hintText:
                'Server which you want to connect (if omit then it default value)',
            labelText: 'Url like http://example.com',
          ),
          autocorrect: false,
          controller: _urlController,
          onChanged: (text) {
            setStringSetting(URL_KEY, text);
          },
        ),
        flex: 1,
      ),
      Expanded(
          child: Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.all(10.0),
              child: RichText(
                textAlign: TextAlign.start,
                text: TextSpan(
                  text: PROFILE_ABOUT_TEXT.replaceAll(RegExp(r'\n|\s+'), ' '),
                  style: PROFILE_ABOUT_STYLE,
                ),
              )))
    ]);
  }
}
