import 'package:bhfront/widgets/log_console_widget.dart';
import 'package:flutter/material.dart';

class LogPage extends StatefulWidget {
  LogPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LogPageState createState() => _LogPageState();
}

class _LogPageState extends State<LogPage> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called
    return Column(children: <Widget>[
      Flexible(
        child: Text('LOG CONSOLE'),
        flex: 1,
      ),
      Flexible(
        child: LogConsoleWidget(
          text: TextSpan(text: ''),
          textAlign: TextAlign.left,
          softWrap: true,
          overflow: TextOverflow.clip,
          textScaleFactor: 1.0,
          textWidthBasis: TextWidthBasis.longestLine,
        ),
        flex: 1,
      )
    ]);
  }
}
