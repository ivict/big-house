class Test {
  final String mnemonic;
  final String name;
  final String content;
  final String summary;

  const Test({this.mnemonic, this.name, this.content, this.summary});


  Test.data(dynamic data) :
      this.mnemonic = data['mnemonic'],
      this.name = data['name'],
      this.content = data['content'],
      this.summary = data['summary'];



}
