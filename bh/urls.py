"""bh URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.models import User
from django.views.static import serve
from rest_framework import routers, serializers, viewsets

# Serializers define the API representation.
from bhcore.views.test_api_view_set import TestAPIViewSet
from bhcore.views.test_details_api_view_set import TestDetailsAPIViewSet
from bhcore.views.test_stage_api_view_set import TestStageAPIViewSet
from bhcore.views.test_stage_history_api_view_set import TestStageHistoryAPIViewSet
from bhcore.views.test_stage_status_api_view_set import TestStageStatusAPIViewSet
from bhcore.views.test_statistics_api_view_set import TestStatisticsAPIViewSet
from bhcore.views.test_statistics_type_api_view_set import TestStatisticsTypeAPIViewSet
from bhcore.views.test_summary_api_view_set import TestSummaryAPIViewSet


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']


# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
# AUTH API
router.register(r'users', UserViewSet)
# BH-CORE API
router.register(r'test', TestAPIViewSet, 'test')
router.register(r'test-details', TestDetailsAPIViewSet, 'test-details')
router.register(r'test-summary', TestSummaryAPIViewSet, 'test-summary')
router.register(r'test-statistics-type', TestStatisticsTypeAPIViewSet, 'test-statistics-type')
router.register(r'test-statistics', TestStatisticsAPIViewSet, 'test-statistics')
router.register(r'test-stage', TestStageAPIViewSet, 'test-stage')
router.register(r'test-stage-status', TestStageStatusAPIViewSet, 'test-stage-status')
router.register(r'test-stage-history', TestStageHistoryAPIViewSet, 'test-stage-history')

urlpatterns = [
    url(r'^api/', include(router.urls)),
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls')),
    # 'django.contrib.staticfiles.views',
    # url(r'^(?:index.html)?$', 'serve', kwargs={'path': 'index.html'}),
    # url(r'^(?P<path>(?:js|css|img)/.*)$', serve, kwargs={
    #         'document_root': 'bhfront/build/web'}),
    url(r'^$', serve, kwargs={
        'path': 'index.html', 'document_root': 'bhfront/build/web'}),
    url(r'^main.dart.js$', serve, kwargs={
        'path': 'main.dart.js', 'document_root': 'bhfront/build/web'}),
    url(r'^main.dart.js$', serve, kwargs={
        'path': 'main.dart.deps', 'document_root': 'bhfront/build/web'}),
    url(r'^main.dart.js$', serve, kwargs={
        'path': 'main.dart.map', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/FontManifest.json$', serve, kwargs={
        'path': 'assets/FontManifest.json', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/packages/cupertino_icons/assets/CupertinoIcons.ttf$', serve, kwargs={
        'path': 'assets/packages/cupertino_icons/assets/CupertinoIcons.ttf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/MaterialIcons-Regular.ttf$', serve, kwargs={
        'path': 'assets/fonts/MaterialIcons-Regular.ttf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/Montserrat-Black.otf$', serve, kwargs={
        'path': 'assets/fonts/Montserrat-Black.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/Montserrat-Bold.otf$', serve, kwargs={
        'path': 'assets/fonts/Montserrat-Bold.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/Montserrat-ExtraBold.otf$', serve, kwargs={
        'path': 'assets/fonts/Montserrat-ExtraBold.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/Montserrat-ExtraLight.otf$', serve, kwargs={
        'path': 'assets/fonts/Montserrat-ExtraLight.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/Montserrat-Light.otf$', serve, kwargs={
        'path': 'assets/fonts/Montserrat-Light.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/Montserrat-Medium.otf$', serve, kwargs={
        'path': 'assets/fonts/Montserrat-Medium.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/Montserrat-Regular.otf$', serve, kwargs={
        'path': 'assets/fonts/Montserrat-Regular.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/Montserrat-SemiBold.otf$', serve, kwargs={
        'path': 'assets/fonts/Montserrat-SemiBold.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/Montserrat-Thin.otf$', serve, kwargs={
        'path': 'assets/fonts/Montserrat-Thin.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-BlackIt.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-BlackIt.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-Black.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-Black.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-BoldIt.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-BoldIt.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-Bold.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-Bold.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-ExtraLightIt.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-ExtraLightIt.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-ExtraLight.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-ExtraLight.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-It.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-It.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-LightIt.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-LightIt.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-Light.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-Light.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-Light.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-Light.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-MediumIt.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-MediumIt.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-Medium.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-Medium.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-Regular.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-Regular.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-SemiboldIt.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-SemiboldIt.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/fonts/SourceCodePro-Semibold.otf$', serve, kwargs={
        'path': 'assets/fonts/SourceCodePro-Semibold.otf', 'document_root': 'bhfront/build/web'}),
    url(r'^assets/assets/favicon.ico$', serve, kwargs={
        'path': 'assets/assets/favicon.ico', 'document_root': 'bhfront/build/web'}),
]
