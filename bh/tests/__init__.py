import sys
import threading

import celery
from django.apps import apps
from django.db import connection
from django.test import TestCase
from django.test import utils

from bh import settings as tests_settings
# CHANGE DEFAULT bh APPLICATION SETTINGS IF POSSIBLE
# settings.configure(default_settings=tests_settings)
from bh.celery import app as current_app

try:
    utils.teardown_test_environment()
    print('TEARDOWN TEST ENVIRONMENT [TEST FLOW IS WRONG?]')
except AttributeError as e:
    print('TEST ENVIRONMENT NOT INITIALIZED IT''S OK')
utils.setup_test_environment(debug=True)
# tests_settings.CELERY_APP = 'test'
# tests_settings.CELERY_BROKER_URL = 'memory://'
# tests_settings.CELERY_BROKER_BACKEND = 'memory'
# tests_settings.CELERY_RESULT_BACKEND = 'rpc://'
# tests_settings.CELERY_CACHE_BACKEND = 'memory'
# tests_settings.CELERY_ALWAYS_EAGER = True
# tests_settings.CELERY_IGNORE_RESULT = False
# tests_settings.CELERY_TRACK_STARTED = True
try:
    apps.populate(tests_settings.INSTALLED_APPS)
except ModuleNotFoundError as e:
    print('TRY TO INSTALL MISSED MODULE {e}'.format(e=e))

TEST_ENVIRONMENT_READY = True


class StreamWrapper(celery.utils.log.LoggingProxy):
    value = None

    def __init__(self, logger, loglevel=None, value=None):
        super(StreamWrapper, self).__init__(logger, loglevel)
        self.value = value

    def getvalue(self):
        return self.value


class EmbeddedWorker(current_app.Worker):
    stdout = None
    stderr = None

    def __init__(self, **kwargs):
        super(EmbeddedWorker, self).__init__(hostname='localhost', pool_cls='prefork', loglevel=10,
                                             logfile=None,  # node format handled by celery.app.log.setup
                                             pidfile=None,  # 'localhost',
                                             statedb=None,  # 'memory',
                                             **kwargs)
        # REPAIR INTERACTION BETWEEN CELERY LOGGER AND JET BRAINS PYCHARM
        self.stdout = sys.stdout
        self.stderr = sys.stderr

    def on_start(self):
        super().on_start()
        sys.stdout = StreamWrapper(logger=sys.stdout.logger, value=self.stdout)
        sys.stderr = StreamWrapper(logger=sys.stderr.logger, value=self.stderr)

    def on_consumer_ready(self, consumer):
        super().on_consumer_ready(consumer)
        TestsBasic.celery_worker_start_condition.acquire()
        TestsBasic.celery_worker_start_condition.notify_all()
        TestsBasic.celery_worker_start_condition.release()


def celery_worker_process(workers):
    kwargs = {'broker': None,
              'result_backend': None,
              'loader': None,
              'config': None,
              'workdir': None,
              'no_color': None,
              'quiet': False,
              'detach': False,
              'optimization': None,
              'prefetch_multiplier': 4,
              'concurrency': 1,
              'pool': 'prefork',
              'task_events': True,
              'time_limit': None,
              'soft_time_limit': None,
              'max_tasks_per_child': None,
              'max_memory_per_child': None,
              'purge': False,
              'queues': [],
              'exclude_queues': [],
              'include': [],
              'without_gossip': False,
              'without_mingle': False,
              'without_heartbeat': False,
              'heartbeat_interval': None,
              'autoscale': None,
              'umask': None,
              'executable': None,
              'beat': False,
              'schedule_filename': 'celerybeat-schedule',
              'scheduler': None,
              }
    worker_instance = EmbeddedWorker(**kwargs)
    workers.append(worker_instance)
    worker_instance.start()


class TestsBasic(TestCase):
    workers = []
    # START CELERY WORKER CONDITION
    celery_worker_start_condition = threading.Condition()

    @classmethod
    def setUpClass(cls):
        test_database_name = connection.creation.create_test_db()
        print('Temporary db [{test_database_name}] created'.format(test_database_name=test_database_name))

        TestsBasic.celery_worker_controller = threading.Thread(target=celery_worker_process, args=(TestsBasic.workers,))
        # START CELERY WORKER
        TestsBasic.celery_worker_start_condition.acquire()
        TestsBasic.celery_worker_controller.start()
        TestsBasic.celery_worker_start_condition.wait()
        TestsBasic.celery_worker_start_condition.release()
        super().setUpClass()

    @classmethod
    def tearDownClass(cls):
        try:
            print('###################################')
            print('worker_instances: ' + str(len(TestsBasic.workers)))
            for worker_instance in TestsBasic.workers:
                worker_instance.stop(5)
            connection.creation.destroy_test_db()
        finally:
            super().tearDownClass()

    def test_simple_check(self):
        self.assertEqual(True, True)
