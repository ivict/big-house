## Requirements
	- DBMS available through docker images
	- Test scenarios can be write on hi-level language or describe in yaml format
    - Test results available in json format for analytic tools processing

	Test Case Example [informal description]
		docker images: oracle nosql (https://hub.docker.com/r/oracle/nosql/)
			kvlite:store oracle/nosql java -jar lib/kvstore.jar runadmin -host store -port 5000 -store kvstore
                describe cluster: two nodes replication 2		

		describe database model: table1, table2, table3
		describe generation of data: random fill table1 (or DML scripts)...

		describe test: insert next N count of data pieces,
			       read inserted data,
		               switch down one node,
			       insert next N count of data pieces,
                               switch up one node,
			       read inserted data

## Design
    Web platform - Django

	Major functions:
	    - Test compose (yaml upload in MVP)
	    - Test run
        - Gathering statistics (json output format in MVP)

	Hi level design components:
		- Cluster deployment engine, docker integration
		- Yaml parser for test description
		- Test agent for start crash scenario and gathering statistics
		- Simple test results storage
		
## Implementation
	Tasks:
         BH-1 # Design ui interface [DONE: see issues/ui-design]
	     BH-2 # Requirements for backend [DONE: see issues/bh-2-issue.txt]
	     BH-3 # Design and implementation data-model [DONE: see issues/bh-3-issue.txt]
	     BH-4 # Design and implementation cluster-deployment engine and docker integration [STAGE 1, DONE: see issues/bh-4-issue.txt]
	     BH-5 # Implementation of ui interface [DONE]
	     BH-6 # Implementation cluster-deployment engine and docker integration [IN PROGRESS]
	     BH-7 # Check and testing for MVP
	        BH-9 # In front interface: move to the new test if system have no tests.
	               If system have one ore more tests move to the test list.
	                    Case 1: no tests and empty local storage => move to the "new test" menu item    
	                    Case 2: one ore more tests and empty local storage => move to the "list of tests" menu item
	        BH-10 # Try to use different rich styled editors from: https://github.com/flutter/flutter/issues/12675
	        BH-11 # Create websocket subscription for update test stage [FRONT]
	        BH-12 # [BUG] front on demohoster can't connect with backend by default (it need push value to profile settings)
	        BH-13 # [BUG] Backed received 403 on try to any post request on demohoster    
	        BH-14 # [BUG] Favicon not show in firefox after clean site cache (reproduced on demohoster)    
	     BH-8 # Bugfixing and reworking

